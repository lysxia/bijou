Bijective Combinatorics
=======================

A formalization of bijective combinatorics in Coq:
counting things by constructing bijections between sets.

# Core definitions

- `Bijection.v`: A bijection `A <--> B` is a pair of inverse functions `A -> B` and `B -> A`.
- `Finordinal.v`: The `n`-th ordinal `fin n` is the "canonical type
   with `n` elements" `{ m : nat | m < n }`.
- `Finite.v`: A finite type is a type `A` together with a bijection `A <--> fin n` for some `n : nat`.
  The number `n` is the *cardinality* of `A`, denoted `#|A|`.
- `Setof.v`: A set in a universe `A` is a predicate `A -> bool`.

# Fun theorems

Inclusion-exclusion (binary) (in `Setof.v`):

```coq
forall X Y : finite_set_of A,
#|X ∪ Y| + #|X ∩ Y| = #|X| + #|Y|
```

Binomial sum (in `Binomial.v`):

```coq
forall n : nat,
2 ^ n = bigsum (fun k => choose n k) (S n)
```

Permutations and factorials (in `Permutation.v`):

```coq
forall A : finite_type,
permutation A <--> fin (factorial #|A|)
```

Partition of a type `A` into the sum of the fibers of a function `A -> B`
(in `Partition.v`):

```coq
forall A B : Type,
A <--> { y : B & { x : A | f x = y } }
```

# Et cetera

- `Powerset.v`: Powerset.
- `Injection.v`: One-sided inverses. Aka. sections. A section `A --> fin n`
  implies a bijection `A <--> fin m` for some `m`, i.e., `A` is finite.
- `Pigeonhole.v`: The pigeonhole principle.
- `Sigma.v`: Sums of types, sums of numbers, and their equivalences.
- `Sets.v`: Types satisfying UIP.
- `Decidable.v`: Decidable propositions.
- `Util.v`: General utilities.
