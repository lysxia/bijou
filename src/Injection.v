From Coq Require Import Arith.
From Equations Require Import Equations.
From Bijou Require Import Util Bijection Fin.

Set Primitive Projections.
Set Implicit Arguments.

(* Split monomorphism: has a semi-inverse *)
Definition is_section {A B : Type} (f : A -> B) (g : B -> option A) : Prop :=
  forall b, g (f b) = Some b.

Record Section A B : Type :=
  { section_to : A -> B
  ; section_from : B -> option A
  ; section_is_section :> is_section section_to section_from
  }.

Definition is_injection {A B : Type} (f : A -> B) : Prop :=
  forall x x', f x = f x' -> x = x'.

Record Injection A B : Type :=
  { inj : A -> B
  ; inj_is_injection :> is_injection inj
  }.

Infix "⊆" := Injection (at level 99) : type_scope.

Lemma section_injection {A B : Type} (f : A -> B) (g : B -> option A)
  : is_section f g -> is_injection f.
Proof.
  intros H x x' Hf. assert (HH : Some x = Some x'); [ | injection HH; auto ].
  rewrite <- (H x), <- (H x'); f_equal; auto.
Defined.

Fixpoint iter_Fin {n : nat} {A : Type} : (fin n -> A -> A) -> A -> A :=
  match n with
  | O => fun _ a => a
  | S n => fun f a => f FO (iter_Fin (fun x => f (FS x)) a)
  end.

Ltac fwd H :=
  let H_ := type of H in
  lazymatch eval hnf in H_ with
  | (?A -> _) =>
    let _tmp_ := fresh in
    assert (_tmp_ : A); [ | specialize (H _tmp_); clear _tmp_ ]
  end.

Lemma iter_Fin_accum {n : nat} {A : Type}
  : forall
      (f : fin n -> A -> A)
      (P : fin n -> A -> Prop)
      (Hadd : forall i x, P i (f i x))
      (Hmon : forall i j x, P i x -> P i (f j x)),
    forall i a, P i (iter_Fin f a).
Proof.
  induction n as [ | n IH ]; intros f P Hadd Hmon i a; cbn.
  - apply (fin_case0 _ i).
  - revert i; apply Fin_caseS; [ apply Hadd | ].
    intros; apply Hmon. apply IH with (i := i); auto.
Defined.

(* TODO?

Section pred_Section.

Context {A : Type} {n : nat} (R : Section A (fin (S n))).

Equations pred_Section_to {a : A} : fin_eqb (@FO n) (section_to R a) = false -> fin n :=
  pred_Section_to p with section_to R a := {
    pred_Section_to p FO := _ ;
    pred_Section_to p (FS i) := i }.

Next Obligation.
  rewrite Nat.eqb_refl in p; discriminate.
Qed.

Equations pred_Section_from : fin n -> option { a : A | Fin.eqb (@FO n) (section_to R a) = false } :=
  pred_Section_from i with section_from R (FS i) := {
    pred_Section_from i None := None ;
    pred_Section_from i (Some a) with Fin_match (section_to R a) := {
      pred_Section_from i (Some a) (F0' _) := None ;
      pred_Section_from i (Some a) (FS' H) := Some (exist _ a _)
    }
  }.

Next Obligation.
  rewrite H in *; reflexivity.
Qed.

Lemma is_section_pred_Section
  : is_section (fun x => pred_Section_to (proj2_sig x)) pred_Section_from.
Proof.
  red; intros; simp pred_Section_to.
  generalize (proj2_sig b).
  remember (section_to _ _) as p eqn:Ep; revert Ep.
  apply (caseS' p); [ | intros i Ei ? ].
  { intros H; exfalso. destruct b. cbn in H. rewrite <- H in e. cbn in e.
    rewrite Nat.eqb_refl in e; discriminate. }
  simp pred_Section_to pred_Section_from.
  rewrite Ei.
  destruct (section_from R _) as [a | ] eqn:E; simp pred_Section_from.
  - rewrite section_is_section in E. injection E; intros <-; clear E.
    destruct Fin_match as [Hj | ]; simp pred_Section_from.
    + pose proof (HH := eq_trans Ei Hj); discriminate.
    + f_equal. apply eq_sig_hprop; [ | reflexivity ]. intros *. apply EqDec.eq_proofs_unicity.
  - rewrite section_is_section in E. discriminate.
Defined.

Definition pred_Section
  : Section {a : A | Fin.eqb (@FO n) (section_to R a) = false} (fin n) :=
  {| section_is_section := is_section_pred_Section |}.

Context (B : Bijection {a : A | Fin.eqb (@FO n) (section_to R a) = false} (fin n)).

Section pred_Section_1.

Context (F : forall a, Fin.eqb (@FO n) (section_to R a) = false).

Definition succ_pred_Section_1_to (a : A) : fin n := bij_to B (exist _ a (F a)).
Definition succ_pred_Section_1_from (i : fin n) : A := proj1_sig (bij_from B i).

Lemma is_bijection_succ_pred_Section_1
  : is_bijection succ_pred_Section_1_to succ_pred_Section_1_from.
Proof.
  unfold succ_pred_Section_1_to, succ_pred_Section_1_from.
  constructor; cbn.
  - intros b; etransitivity; [ | eapply (fg B) ]. f_equal.
    apply eq_sig_hprop; [ intros; apply EqDec.eq_proofs_unicity | reflexivity ].
  - intros; rewrite (gf B). reflexivity.
Defined.

Definition Bij_succ_pred_Section_1 : A <--> fin n :=
  {| bij_is_bijection := is_bijection_succ_pred_Section_1 |}.

End pred_Section_1.

Section pred_Section_2.

Context (a0 : A) (F : section_to R a0 = FO).

Equations succ_pred_Section_2_to : A -> fin (S n) :=
  | a with Fin_match (section_to R a) :=
    | (F0' _) := FO ;
    | (FS' H) := FS (bij_to B (exist _ a _)).

Next Obligation.
  rewrite H; auto.
Qed.

Equations succ_pred_Section_2_from : fin (S n) -> A :=
  | FO := a0 ;
  | (FS i) := proj1_sig (bij_from B i).

Lemma is_bijection_succ_pred_Section_2
  : is_bijection succ_pred_Section_2_to succ_pred_Section_2_from.
Proof.
  constructor; intros x.
  - apply caseS' with (p := x).
    + simp succ_pred_Section_2_from succ_pred_Section_2_to.
      destruct Fin_match; simp succ_pred_Section_2_to.
      { reflexivity. }
      rewrite e in F; discriminate.
    + intros p; simp succ_pred_Section_2_from succ_pred_Section_2_to.
      destruct Fin_match; simp succ_pred_Section_2_to.
      * assert (JJ := proj2_sig (bij_from B p)); cbn in JJ, e. rewrite e in JJ.
        rewrite Nat.eqb_refl in JJ. discriminate.
      * f_equal. etransitivity; [ | eapply (fg B) ]. f_equal.
        apply eq_sig_hprop; [ intros; apply EqDec.eq_proofs_unicity | reflexivity ].
  - simp succ_pred_Section_2_to.
    destruct Fin_match; simp succ_pred_Section_2_to succ_pred_Section_2_from.
    + rewrite <- e in F. revert F; apply (section_injection R).
    + rewrite (gf B). reflexivity.
Defined.

Definition Bij_succ_pred_Section_2 : A <--> fin (S n) :=
  {| bij_is_bijection := is_bijection_succ_pred_Section_2 |}.

End pred_Section_2.

Section Finite_Section.

(* Context {A : Type} {n : nat} (R : Section A (fin n)). *)

(* Definition has_antecedent : fin n -> bool := *)



End Finite_Section.

End pred_Section.
 *)
