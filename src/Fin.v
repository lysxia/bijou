(** * Finite ordinals *)

(** Finite ordinals provide a basis for enumerating finite sets. *)
(** They are isomorphic to [fin n := { m | m < n }]. *)

From Coq Require Import Arith Vector Lia.
From Equations Require Import Equations.
From Bijou Require Import Bijection.

Set Implicit Arguments.
Set Primitive Projections.

Definition fin (n : nat) := { i : nat | i < n }.

(** * General facts about [fin] *)

Lemma equate_fin {n : nat} (i j : fin n) : proj1_sig i = proj1_sig j -> i = j.
Proof.
  destruct i, j; cbn. intros <-. f_equal. apply le_unique.
Qed.

Definition FO {n : nat} : fin (S n) := exist _ 0 (Nat.lt_0_succ n).
Definition FS {n : nat} (i : fin n) : fin (S n) :=
  exist _ (S (proj1_sig i)) (proj1 (Nat.succ_lt_mono _ _) (proj2_sig i)).

Lemma Fin_caseS {n} {P : fin (S n) -> Type} (H0 : P FO) (HS : forall i, P (FS i)) : forall i, P i.
Proof.
  intros [[] H].
  - erewrite (le_unique _ _ H); apply H0.
  - erewrite (le_unique _ _ H). apply (HS (exist _ n0 (proj2 (Nat.succ_lt_mono _ _) H))).
Qed.

Lemma Fin_caseS_ {n} {P : Type} (H0 : P) (HS : fin n -> P) : fin (S n) -> P.
Proof.
  intros [[] H].
  - apply H0.
  - apply (HS (exist _ n0 (proj2 (Nat.succ_lt_mono _ _) H))).
Defined.

Definition sum_to_l {n m} (i : fin n) : fin (n + m) :=
  exist _ (proj1_sig i) (Nat.lt_lt_add_r _ _ _ (proj2_sig i)).

Definition sum_to_r {n m} (i : fin m) : fin (n + m) :=
  exist _ (n + proj1_sig i) (proj1 (Nat.add_lt_mono_l _ _ _) (proj2_sig i)).

Definition sum_to {n m} (i : fin n + fin m) : fin (n + m) :=
  match i with
  | inl il => sum_to_l il
  | inr ir => sum_to_r ir
  end.

Lemma sum_from_lemma {i n m : nat} (H : i < n + m) (H1 : n <= i) : i - n < m.
Proof. lia. Qed.

Definition sum_from {n m} (i : fin (n + m)) : fin n + fin m :=
  match le_lt_dec n (proj1_sig i) with
  | left H => inr (exist _ (proj1_sig i - n) (sum_from_lemma (proj2_sig i) H))
  | right H => inl (exist _ (proj1_sig i) H)
  end.

Lemma sum_to_from_l {n m} : forall (i : fin n), sum_from (m := m) (sum_to_l i) = inl i.
Proof.
  intros i. unfold sum_from, sum_to_l; cbn [proj1_sig proj2_sig].
  destruct (le_lt_dec _ _).
  - destruct i; cbn in *. lia.
  - destruct i; cbn; f_equal. f_equal. apply le_unique.
Qed.

Lemma sum_to_from_r {n m} : forall (i : fin m), sum_from (n := n) (sum_to_r i) = inr i.
Proof.
  intros i. unfold sum_from, sum_to_r; cbn [proj1_sig proj2_sig].
  destruct (le_lt_dec _ _).
  - destruct i; cbn; f_equal. apply equate_fin; cbn. lia.
  - destruct i; cbn in *; lia.
Qed.

Lemma sum_to_from {n m} : forall (i : fin n + fin m), sum_from (sum_to i) = i.
Proof.
  destruct i as [i | i]; cbn; auto using sum_to_from_l, sum_to_from_r.
Qed.

Lemma sum_from_to {n m} : forall (i : fin (n + m)), sum_to (sum_from i) = i.
Proof.
  intros i. unfold sum_from. destruct le_lt_dec.
  - cbn. unfold sum_to_r; cbn. apply equate_fin; cbn. lia.
  - cbn. unfold sum_to_l; cbn. apply equate_fin; cbn. reflexivity.
Qed.

Inductive Fin_pat {n} (y : fin (S n)) : Type :=
| F0' : y = FO -> Fin_pat y
| FS' z : y = FS z -> Fin_pat y
.

Definition Fin_match {n} : forall (y : fin (S n)), Fin_pat y.
Proof.
  destruct y as [i H].
  destruct i as [ | i ].
  - apply F0'. apply equate_fin. reflexivity.
  - apply (FS' (z := exist _ i (proj2 (Nat.succ_lt_mono _ _) H))).
    apply equate_fin. reflexivity.
Defined.

Definition Fin_to_nat {n} (i : fin n) : nat := proj1_sig i.

Coercion Fin_to_nat : fin >-> nat.

Definition Fin_to_nat_bounded {n : nat} (i : fin n) : i < n := proj2_sig i.

Lemma Fin_to_nat_bounded_S {n : nat} (i : fin (S n)) : i <= n.
Proof.
  apply Nat.lt_succ_r, Fin_to_nat_bounded.
Qed.

Notation Absurd := (False_rect _).

Definition nat_to_Fin {n m : nat} : n < m -> fin m := exist _ n.

Definition nat_to_Fin_to_nat {n m : nat} (n_lt_m : n < m)
  : Fin_to_nat (nat_to_Fin n_lt_m) = n :=
  eq_refl.

Definition nat_to_Fin' (n m : nat) : { i : fin m | n = Fin_to_nat i } + { m <= n }.
Proof.
  destruct (le_lt_dec m n).
  - right. auto.
  - left. exists (nat_to_Fin l). reflexivity.
Defined.

Definition eq_exist_irrel {A} (P : A -> Prop) (H : forall x (p q : P x), p = q) (x y : sig P)
  : proj1_sig x = proj1_sig y -> x = y.
Proof.
  destruct x, y; cbn; intros <-; f_equal; apply H.
Defined.

Lemma Fin_to_nat_to_Fin' (n : nat) (i : fin n)
  : nat_to_Fin' (Fin_to_nat i) n = inleft (exist _ i eq_refl).
Proof.
  unfold nat_to_Fin'.
  destruct le_lt_dec.
  - destruct i; cbn in *; lia.
  - destruct i; cbn. f_equal.
    apply eq_exist_irrel; [ intros; apply UIP_nat | cbn ].
    apply equate_fin; reflexivity.
Qed.

Definition unwrap_sumor {A B} (H : ~B) (x : A + {B}) : A :=
  match x with
  | inleft i => i
  | inright i => match H i with end
  end.

Lemma nat_to_Fin_to_nat' {n m : nat} (H : ~ m <= n)
  : Fin_to_nat (proj1_sig (unwrap_sumor H (nat_to_Fin' n m))) = n.
Proof.
  unfold nat_to_Fin'.
  destruct le_lt_dec.
  - contradiction.
  - reflexivity.
Qed.

Lemma Fin_to_nat_to_Fin {n : nat} (i : fin n)
  : nat_to_Fin (Fin_to_nat_bounded i) = i.
Proof.
  destruct i; reflexivity.
Defined.

Lemma injective_Fin_to_nat {n} {i j : fin n} : Fin_to_nat i = Fin_to_nat j -> i = j.
Proof. apply equate_fin. Defined.

(** * Injectivity of [fin] *)

Lemma fin_0_contra : fin 0 -> False.
Proof.
  intros [? H]. apply (Nat.nlt_0_r _ H).
Qed.

Lemma fin_case0 (A : Type) : fin 0 -> A.
Proof. intros i; destruct (fin_0_contra i). Qed.

Lemma Fin_O_S_contra : forall n, (fin O <--> fin (S n)) -> False.
Proof.
  intros n B. exact (fin_0_contra (bij_from B FO)).
Qed.

Section Fin_injective.

Section Fin_S_injective_1.

Context {n m : nat} (B : fin (S n) <--> fin (S m)).

Lemma Fin_S_injective__contra (x : fin n) :
  bij_to B (FS x) = FO -> bij_to B FO = FO -> False.
Proof.
  intros H1 H2.
  apply (proj1 (is_bijection_adjunct _ _) B) in H1.
  rewrite <- H2 in H1. rewrite (gf B) in H1. discriminate.
Qed.

Equations Fin_S_injective__to : fin n -> fin m :=
  Fin_S_injective__to x with Fin_match (bij_to B (FS x)) := {
    Fin_S_injective__to x (@FS' y _) := y ;
    Fin_S_injective__to x (F0' Hx) with Fin_match (bij_to B FO) := {
      Fin_S_injective__to x (F0' Hx) (@FS' y _) := y ;
      Fin_S_injective__to x (F0' Hx) (F0' Hy) with Fin_S_injective__contra x Hx Hy := {}
    }
  }.

End Fin_S_injective_1.

Lemma FS_inj {n : nat} (i j : fin n) : FS i = FS j -> i = j.
Proof.
  intros H. apply equate_fin.
  apply (f_equal (fun x => proj1_sig x)) in H. cbn in H.
  auto.
Qed.

Lemma fg_Fin_S_injective {n m : nat} (B : fin (S n) <--> fin (S m))
  : forall x : fin m,
      Fin_S_injective__to B (Fin_S_injective__to (Bij_dual B) x) = x.
Proof.
  intros x. simp Fin_S_injective__to.
  destruct (Fin_match (_ (FS x))) as [ Hx | x_ Hx ]; simp Fin_S_injective__to.
  - destruct (Fin_match _) as [ | y Hy ]; simp Fin_S_injective__to.
    + destruct Fin_S_injective__contra.
    + destruct (Fin_match _) as [ Hy' | y' Hy' ]; simp Fin_S_injective__to.
      * destruct (Fin_match _) as [ | x' Hx' ]; simp Fin_S_injective__to.
        { destruct Fin_S_injective__contra. }
        rewrite <- Hx, (fg B) in Hx'.
        auto using FS_inj.
      * rewrite <- Hy, (fg B) in Hy'. discriminate.
  - destruct (Fin_match _) as [ Hy' | y' Hy' ]; simp Fin_S_injective__to.
    + destruct (Fin_match _) as [ | x' Hx' ]; simp Fin_S_injective__to.
      * destruct Fin_S_injective__contra.
      * rewrite <- Hx, (fg B) in Hy'. discriminate.
    + rewrite <- Hx, (fg B) in Hy'. auto using FS_inj.
Qed.

Lemma is_bijection_Fin_S_injective {n m : nat} (B : fin (S n) <--> fin (S m))
  : is_bijection (Fin_S_injective__to B) (Fin_S_injective__to (Bij_dual B)).
Proof.
  constructor; apply fg_Fin_S_injective.
Qed.

Definition Fin_S_injective {n m : nat} (B : fin (S n) <--> fin (S m))
  : fin n <--> fin m :=
  {| bij_is_bijection := is_bijection_Fin_S_injective B |}.

Definition Fin_injective : forall n m, (fin n <--> fin m) -> n = m.
Proof.
  induction n as [ | n IH ]; intros [].
  - reflexivity.
  - intros H; destruct (Fin_O_S_contra H).
  - intros H; destruct (Fin_O_S_contra (Bij_dual H)).
  - intros H; f_equal; apply IH, Fin_S_injective, H.
Defined.

End Fin_injective.

(** ** Empty *)

Definition Bij_0 {A} (H : A -> False) : A <--> fin 0 :=
  Bij_Empty H (fin_case0 _).

(** ** Singletons *)

Lemma singleton_Fin_1 : forall i : fin 1, i = FO.
Proof.
  intros []; apply equate_fin; cbn. inversion l; auto. inversion H0.
Qed.

Section Singleton.

Context {A : Type} (a : A) (H : forall a' : A, a = a').

Lemma is_bijection_singleton : is_bijection (fun _ : A => FO) (fun _ : fin 1 => a).
Proof.
  constructor; auto using singleton_Fin_1.
Defined.

Definition Bij_singleton : A <--> fin 1 :=
  {| bij_is_bijection := is_bijection_singleton |}.

End Singleton.

Definition Bij_unit : unit <--> fin 1 :=
  Bij_singleton (fun 'tt => eq_refl).

(** ** Booleans *)

Definition bool_to (b : bool) : fin 2 :=
  match b with
  | false => exist _ 0 Nat.lt_0_2
  | true => exist _ 1 Nat.lt_1_2
  end.

Definition bool_from : fin 2 -> bool := fun i =>
  match proj1_sig i with
  | O => false
  | S _ => true
  end.

Lemma bool_is_bijection : is_bijection bool_to bool_from.
Proof.
  constructor.
  - intros [[ | [ | ]] H]; [ | | lia ]; apply equate_fin; reflexivity.
  - intros []; reflexivity.
Qed.

Definition Bij_bool : bool <--> fin 2 :=
  {| bij_is_bijection := bool_is_bijection |}.

Definition Bij_Fin_sum {n m} : Bijection (fin n + fin m) (fin (n + m)) :=
  {| bij_is_bijection := {| fg := sum_from_to ; gf := sum_to_from |} |}.

Equations S_to {n} : fin (S n) -> unit + fin n :=
S_to (exist _ O _) := inl tt ;
S_to (exist _ (S i) H) := inr (exist _ i (proj2 (Nat.succ_lt_mono _ _) H)).

Definition S_from {n} (i : unit + fin n) : fin (S n) :=
  match i with
  | inl _ => FO
  | inr i => FS i
  end.

Lemma S_is_bijection {n} : is_bijection (@S_to n) S_from.
Proof.
  constructor.
  - intros [[] | ]; unfold S_from, FO, FS; simp S_to; [ reflexivity | ].
    f_equal; apply equate_fin; reflexivity.
  - apply Fin_caseS; intros; unfold FO, FS; cbn; simp S_to; [ reflexivity | ].
    cbn. apply equate_fin; reflexivity.
Qed.

Definition Bij_S {n} : fin (S n) <--> unit + fin n :=
  {| bij_is_bijection := S_is_bijection |}.

Lemma to_fin_prod_lemma {n m i j} (Hi : i < n) (Hj : j < m) : i * m + j < n * m.
Proof.
  replace n with (i + (n - i)) by lia.
  rewrite Nat.mul_add_distr_r.
  apply Nat.add_lt_mono_l.
  apply (Nat.lt_le_trans _ _ _ Hj).
  replace m with (1 * m) at 1 by lia.
  apply Nat.mul_le_mono_r.
  lia.
Qed.

Definition to_fin_prod {n m} (i : fin n * fin m) : fin (n * m) :=
  let '(exist _ i Hi, exist _ j Hj) := i in
  exist _ (i * m + j) (to_fin_prod_lemma Hi Hj).

Lemma from_fin_prod_lemma1 {n m i : nat} (H : i < n * m) : i / m < n.
Proof. apply Nat.Div0.div_lt_upper_bound. lia. Qed.

Lemma from_fin_prod_lemma2 {n m i : nat} (H : i < n * m) : i mod m < m.
Proof. apply Nat.mod_upper_bound. lia. Qed.

Definition from_fin_prod {n m} (i : fin (n * m)) : fin n * fin m :=
  let '(exist _ i Hi) := i in
  (exist _ (i / m) (from_fin_prod_lemma1 Hi), exist _ (i mod m) (from_fin_prod_lemma2 Hi)).

Lemma to_from_fin_prod {n m} (i : fin (n * m))
  : to_fin_prod (from_fin_prod i) = i.
Proof.
  destruct i; cbn. apply equate_fin; cbn.
  pose proof (Nat.div_mod_eq x m).
  lia.
Qed.

Lemma from_to_fin_prod {n m} (i : fin n * fin m) : from_fin_prod (to_fin_prod i) = i.
Proof.
  destruct i as [ [] [] ]; cbn; f_equal; apply equate_fin; cbn.
  - rewrite Nat.div_add_l by lia. rewrite Nat.div_small; lia.
  - rewrite Nat.add_comm, Nat.Div0.mod_add. rewrite Nat.mod_small; lia.
Qed.

Definition is_bijection_fin_prod {n m} : is_bijection (@to_fin_prod n m) (@from_fin_prod n m) :=
  {| fg := to_from_fin_prod ; gf := from_to_fin_prod |}.

Definition Bij_Fin_prod {n m} : fin n * fin m <--> fin (n * m) :=
  {| bij_is_bijection := is_bijection_fin_prod |}.

(** ** Vectors *)

Delimit Scope vector_scope with vector.
Bind Scope vector_scope with Vector.t.

Arguments Vector.nil {A}.
Arguments Vector.cons {A} _ {n}.

Notation "'[' ']'" := Vector.nil : vector_scope.
Infix "::" := Vector.cons : vector_scope.

Section Vector_cons.

Context {A : Type} {n : nat}.

Lemma is_bijection_Vector_cons
  : @is_bijection (A * Vector.t A n) (Vector.t A (S n))
      (fun xy => Vector.cons (fst xy) (snd xy))
      (fun xxs => (Vector.hd xxs, Vector.tl xxs)).
Proof.
  constructor.
  - intros xxs; apply (Vector.caseS' xxs); reflexivity.
  - intros []; reflexivity.
Defined.

Definition Bij_Vector_cons : A * Vector.t A n <--> Vector.t A (S n) :=
  {| bij_is_bijection := is_bijection_Vector_cons |}.

End Vector_cons.

(** Congruence for [fin] *)
Definition Bij_Fin_eq {n m} (p : n = m) : Bijection (fin n) (fin m) :=
  eq_rect _ (fun m => Bijection (fin n) (fin m)) Bij_id _ p.

Definition fin_eqb {n : nat} (i j : fin n) : bool :=
  Nat.eqb (proj1_sig i) (proj1_sig j).

Definition fin_eq_dec {n : nat} (i j : fin n) : {i = j} + {i <> j}.
Proof.
  destruct (Nat.eq_dec (proj1_sig i) (proj1_sig j)).
  - left. apply equate_fin. auto.
  - right. intros <-; apply (n0 eq_refl).
Defined.
