(** * Binomial coefficients *)

(** Binomial coefficents [choose n k] are the number of
    [k]-subsets of a set of size [n] ([subset n k]).
*)

(** [binomial_sum] theorem: [2^k] equals the sum of [choose n k].

    Here we give a combinatorial proof, constructing a bijection between
    [powerset n] and the sum of [subset n k] using the paritition theorem
    [Partition.sig_fiber].
*)

From Coq Require Import Vector Arith Lia ssreflect.
From Equations Require Import Equations.
From Bijou Require Import Bijection Fin Finite Powerset Partition Sigma Sets.

Import EqNotations.

(** * Definitions *)

(** Binomial coefficients *)
Fixpoint choose (n k : nat) : nat :=
  match n, k with
  | _, O => 1
  | O, S _ => 0
  | S n', S k' => choose n' (S k') + choose n' k'
  end.

(** The type of [k]-subsets of a [n]-set. *)
Definition subset_ (n k : nat) : Type :=
  { x : powerset n | size_powerset x = k }.

(** [subset_ n k] is the carrier of the corresponding [finite_type], [subset n k].

    The remaining component is an equivalence between [subset_ n k] and [choose n k]
    ([subset_choose]). *)

(** [subset_choose] is defined by induction on [n]. There are three cases depending on
    the patterns of [(n, k)], mirroring the definition of [choose]. *)

Lemma unique_empty_subset n : forall (a : {x : powerset n | size_powerset x = 0}),
    exist (fun x : powerset n => size_powerset x = 0) (const false n) (size_const_false n) = a.
Proof.
  move=> [x Hx]. apply eq_exist_irrel. intros; apply set_uip.
  move: Hx; elim x=> [| [] ? ? ]; cbn; [ reflexivity | discriminate | ].
  move=> H0 H1; f_equal; auto.
Qed.

(** [subset_choose] for [k = 0] *)
Definition subset_n_0 {n : nat} : subset_ n 0 <--> fin 1 :=
  Bij_singleton (unique_empty_subset _).

Lemma subset_0_k' {k : nat} : subset_ 0 (S k) -> False.
Proof.
  move=> []. refine (Vector.case0 _ _); cbn; discriminate.
Qed.

(** [subset_choose] for [n = 0] *)
Definition subset_0_k {k : nat} : subset_ 0 (S k) <--> False :=
  Bij_False subset_0_k'.

Lemma bij_eq_S {n k} : (S n = S k) <--> (n = k).
Proof.
  apply bij_unique_Prop.
  - apply Nat.succ_inj_wd.
  - intros; apply set_uip.
  - intros; apply set_uip.
Qed.

(** [subset_choose] for [n = S n'] and [k = S k'] *)
Definition subset_S_S {n k : nat}
  : subset_ (S n) (S k) <--> subset_ n (S k) + subset_ n k :=
  Bij_proj1_sig powerset_S >>> Bij_sig_sum >>> sum_comm
    >>> Bij_sum Bij_id (Bij_proj2_sig (fun _ => bij_eq_S)).

Fixpoint subset_choose n k : subset_ n k <--> fin (choose n k) :=
  match n, k with
  | _, O => subset_n_0
  | O, S _ => subset_0_k >>> enum False_finite
  | S n', S k' =>
    subset_S_S >>> Bij_sum (subset_choose n' (S k')) (subset_choose n' k') >>> Bij_Fin_sum
  end.

Definition is_finite_subset n k : is_finite (subset_ n k) :=
  {| enum := subset_choose n k |}.

(** The [finite_type] of [k]-subsets of an [n]-set. *)
Definition subset (n k : nat) : finite_type :=
  {| ft_is_finite := is_finite_subset n k |}.

(** * Binomial sum theorem *)

(** Bijection between [powerset n] and the sum of [subset n k],
    by the partition theorem. *)
Definition powerset_subset n : powerset n <--> { k : nat & subset n k } :=
  sig_fiber size_powerset.

(** The cardinality of [powerset n] is [2^n] by definition.
    The remaining obligation concerns the sum, to prove that it is bounded:
    [subset n k] is empty if [S n <= k]. *)

Lemma subset_bounded n k : subset_ n k -> k <= n.
Proof.
  intros [x Hx].
  revert k Hx; induction x as [ | [] ? ]; cbn; intros k Hx; [ subst; auto | | ].
  - subst k; apply le_n_S, IHx, eq_refl.
  - subst k; apply le_S, IHx, eq_refl.
Qed.

Lemma subset_bounded' n k : subset_ n k -> S n <= k -> False.
Proof.
  intros Hss Hle; apply subset_bounded in Hss. revert Hle Hss. apply Nat.lt_nge.
Qed.

Lemma sum_subset_choose n : { k : nat & subset n k } <--> fin (bigsum (choose n) (S n)).
Proof.
  apply enum_bounded_sigT, subset_bounded'.
Defined.

Theorem binomial_sum n : 2 ^ n = bigsum (choose n) (S n).
Proof.
  apply Fin_injective.
  apply (Bij_compose (Bij_dual (enum (powerset n)))).
  apply (Bij_compose (powerset_subset _)).
  apply sum_subset_choose.
Qed.
