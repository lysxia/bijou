From Coq Require Import Arith Lia.
From Bijou Require Import Fin Bijection Finite Sigma.

Set Implicit Arguments.
Set Primitive Projections.

Infix "**" := prod (at level 40) : type_scope.

Record species : Type :=
  { sp_type :> Type
  ; sp_size : sp_type -> nat
  ; sp_finite : forall n, is_finite { x : sp_type | n = sp_size x }
  }.

Declare Scope species_scope.
Delimit Scope species_scope with species.
Bind Scope species_scope with species.

Arguments sp_type _%_species.
Arguments sp_size _%_species.
Arguments sp_finite _%_species.

(* Representation of species as a power series *)
Record series : Type :=
  { sr_type :> nat -> finite_type }.

Definition sp_sized (F : species) : series :=
  {| sr_type := fun (n : nat) =>
      {| ft_type := { x : sp_type F | n = sp_size F x }
      ;  ft_is_finite := sp_finite F n |}
  |}.

Coercion sp_sized : species >-> series.

Record species_eq (F G : species) : Type :=
  { sp_bij : Bijection F G
  ; sp_bij_size : forall x, sp_size G (bij_to sp_bij x) = sp_size F x
  }.

Theorem Sized_equate {A : Type} {size : A -> nat} {n : nat} (xy xy' : { x : A | n = size x })
  : proj1_sig xy = proj1_sig xy' -> xy = xy'.
Proof.
  destruct xy, xy'; cbn; intros <-.
  f_equal.
  apply UIP_nat.
Qed.

(** * Unit species *)

Notation sized_id n := { x : unit | n = 1 }.

Lemma singleton_sized_id {n : nat} (H : n = 1) : forall a, exist (fun _ => n = 1) tt H = a.
Proof.
  destruct a as [ [] ? ]. f_equal. apply UIP_nat.
Qed.

Definition is_finite_sized_id (n : nat) : is_finite (sized_id n) :=
  match Nat.eq_dec n 1 with
  | left e => singleton_is_finite (singleton_sized_id e)
  | right e => absurd_is_finite (fun x => e (proj2_sig x))
  end.

Definition id_species : species :=
  {| sp_type := unit
   ; sp_size := fun _ => 1
   ; sp_finite := is_finite_sized_id
  |}.

(** * Empty species *)

Definition zero_species : species :=
  {| sp_type := False
   ; sp_size := fun _ => 0
   ; sp_finite := fun _ => {| card := 0 ; enum := Bij_0 (fun x => proj1_sig x) |}
  |}.

(** * Product *)

(* Type of sized values of (F * G) *)
Notation sized_product F G n :=
  { x : F%species * G%species | n = sp_size F (fst x) + sp_size G (snd x) }.

(* We show that sized_product is finite by constructing a bijection with the convolution formula. *)

(* Convolution is defined as an operation on series. *)
Definition convolution (F G : series) : series :=
  {| sr_type := fun n => sigT_finite (finite_fin (S n)) (fun (i : fin (S n)) => prod_finite (F i) (G (n - i))) |}.

Theorem convolution_equate {F G : species} (n : nat) (xy xy' : convolution F G n)
  : projT1 xy = projT1 xy' ->
    proj1_sig (fst (projT2 xy)) = proj1_sig (fst (projT2 xy')) ->
    proj1_sig (snd (projT2 xy)) = proj1_sig (snd (projT2 xy')) ->
    xy = xy'.
Proof.
  destruct xy as [? [[] []]], xy' as [? [[] []]]; cbn.
  intros <- <- <-.
  repeat f_equal; apply UIP_nat.
Qed.

Definition from_convolution_ {F G : species} {n : nat} (xy : convolution F G n)
  : F ** G :=
  ( proj1_sig (fst (projT2 xy)), proj1_sig (snd (projT2 xy)) ).

Lemma add_complement (n : nat) (i : fin (S n)) : n = i + (n - i).
Proof.
  assert (i <= n) by apply Fin_to_nat_bounded_S.
  lia.
Qed.

Lemma from_convolution_size {F G : species} {n : nat} (xy : convolution F G n)
  : n = sp_size F (fst (from_convolution_ xy)) + sp_size G (snd (from_convolution_ xy)).
Proof.
  cbn [fst snd from_convolution_].
  rewrite <- (proj2_sig (fst (projT2 xy))).
  rewrite <- (proj2_sig (snd (projT2 xy))).
  apply add_complement.
Qed.

Definition from_convolution {F G : species} {n : nat} (xy : convolution F G n)
  : sized_product F G n :=
  exist _ (from_convolution_ xy) (from_convolution_size xy).

Lemma sized_product_bound1 {F G : species} {n : nat} (xy : sized_product F G n)
  : sp_size F (fst (proj1_sig xy)) < S n.
Proof.
  rewrite (proj2_sig xy) at 1. lia.
Qed.

Notation size1 xy := (nat_to_Fin (sized_product_bound1 xy)).

Lemma sized_product_size1 {F G : species} {n : nat} (xy : sized_product F G n)
  : Fin_to_nat (size1 xy) = sp_size F (fst (proj1_sig xy)).
Proof.
  apply nat_to_Fin_to_nat.
Qed.

Lemma sized_product_size2 {F G : species} {n : nat} (xy : sized_product F G n)
  : n - size1 xy = sp_size G (snd (proj1_sig xy)).
Proof.
  rewrite nat_to_Fin_to_nat.
  apply Nat.add_sub_eq_r.
  rewrite Nat.add_comm.
  symmetry.
  apply (proj2_sig xy).
Qed.

Definition to_convolution {F G : species} {n : nat} (xy : sized_product F G n)
  : convolution F G n :=
  existT _ (size1 xy)
    ( exist _ (fst (proj1_sig xy)) (sized_product_size1 xy)
    , exist _ (snd (proj1_sig xy)) (sized_product_size2 xy)).

Lemma from_to_convolution {F G : species} {n : nat} (xy : sized_product F G n)
  : from_convolution (to_convolution xy) = xy.
Proof.
  apply Sized_equate.
  destruct xy as [ [] ? ].
  reflexivity.
Qed.

Lemma to_from_convolution {F G : species} {n : nat} (xy : convolution F G n)
  : to_convolution (from_convolution xy) = xy.
Proof.
  apply convolution_equate.
  - apply injective_Fin_to_nat.
    transitivity (sp_size F (proj1_sig (fst (projT2 xy)))).
    + unfold to_convolution; cbn [projT1].
      rewrite nat_to_Fin_to_nat. reflexivity.
    + symmetry. apply (proj2_sig (fst (projT2 xy))).
  - reflexivity.
  - reflexivity.
Qed.

Definition is_bijection_convolution {F G : species} {n : nat}
  : is_bijection (@from_convolution F G n) (@to_convolution F G n)
  := {| fg := from_to_convolution ; gf := to_from_convolution |}.

Definition convolution_product_species (F G : species) (n : nat)
  : convolution F G n <--> sized_product F G n :=
  {| bij_to := from_convolution
   ; bij_from := to_convolution
   ; bij_is_bijection := is_bijection_convolution |}.

Definition is_finite_sized_product (F G : species) (n : nat) : is_finite (sized_product F G n) :=
  is_finite_bij (convolution_product_species F G n) (convolution F G n).

Definition product_species (F G : species) : species :=
  {| sp_type := F * G
  ;  sp_size := fun xy => sp_size F (fst xy) + sp_size G (snd xy)
  ;  sp_finite := is_finite_sized_product F G
  |}.

Infix "*" := product_species : species_scope.

(** * Sum species *)

Definition sum_size (F G : species) (x : F + G) : nat :=
  match x with
  | inl x => sp_size F x
  | inr y => sp_size G y
  end.

(* Type of sized values of (F + G) *)
Notation sized_sum F G n := { x : F%species + G%species | n = sum_size F G x }.

Definition sum_series (F G : series) : series :=
  {| sr_type := fun n => (F n + G n)%finite |}.

Definition to_sum_series {F G : species} {n : nat} (x : sized_sum F G n) : sum_series F G n :=
  match x with
  | exist _ (inl x) e => inl (exist _ x e)
  | exist _ (inr x) e => inr (exist _ x e)
  end.

Definition from_sum_series {F G : species} {n : nat} (x : sum_series F G n) : sized_sum F G n :=
  match x with
  | inl (exist _ x e) => exist _ (inl x) e
  | inr (exist _ x e) => exist _ (inr x) e
  end.

Definition to_from_sum_series {F G : species} {n : nat} (x : sum_series F G n)
  : to_sum_series (from_sum_series x) = x :=
  match x with
  | inl (exist _ x e) | inr (exist _ x e) => eq_refl
  end.

Definition from_to_sum_series {F G : species} {n : nat} (x : sized_sum F G n)
  : from_sum_series (to_sum_series x) = x.
Proof.
  destruct x as [ [] ?]; reflexivity.
Defined.

Definition is_bijection_sum_series_species (F G : species) (n : nat)
  : is_bijection (@from_sum_series F G n) (@to_sum_series F G n) :=
  {| fg := from_to_sum_series ; gf := to_from_sum_series |}.

Definition bij_sum_series_species (F G : species) (n : nat)
  : sum_series F G n <--> sized_sum F G n :=
  {| bij_to := from_sum_series
   ; bij_from := to_sum_series
   ; bij_is_bijection := is_bijection_sum_series_species F G n |}.

Definition sized_sum_is_finite (F G : species) (n : nat) : is_finite (sized_sum F G n) :=
  is_finite_bij (bij_sum_series_species F G n) (sum_series F G n).

Definition sum_species (F G : species) : species :=
  {| sp_type := F + G
   ; sp_size := sum_size F G
   ; sp_finite := sized_sum_is_finite F G
  |}.

(** * Pointing *)

(** Pointing corresponds to the derivative of series. *)

(* d/dx(sum(C(i) x^i)) = sum((i+1) C(i+1) x^i) *)
Definition deriv_series (C : series) : series :=
  {| sr_type := fun n => (finite_fin (S n) * C (S n))%finite |}.

Notation sized_point F n :=
  { x : { a : F%species & fin (sp_size F a) } | n = pred (sp_size F (projT1 x)) }.

Definition _cast {n m : nat} (p : n = pred m) (i : fin m) : fin (S n).
Proof.
  exists (proj1_sig i).
  rewrite p. destruct i; cbn. lia.
Defined.

Definition _uplift {n m : nat} (p : n = pred m) (i : fin m) : S n = m.
Proof.
  destruct (eq_sym p); clear p.
  destruct i as [? ?]. lia.
Qed.

Definition to_deriv_series {F : species} {n : nat} (x : sized_point F n)
  : deriv_series F n :=
  ( _cast (proj2_sig x) (projT2 (proj1_sig x))
  , exist _ (projT1 (proj1_sig x)) (_uplift (proj2_sig x) (projT2 (proj1_sig x)))).

Definition __cast {n m : nat} (p : S n = m) (i : fin (S n)) : fin m.
Proof.
  exists (proj1_sig i).
  destruct p. apply (proj2_sig i).
Defined.

Definition __lower {n m : nat} (p : S n = m) : n = pred m :=
  match p with
  | eq_refl => eq_refl
  end.

Definition from_deriv_series {F : species} {n : nat} (x : deriv_series F n)
  : sized_point F n :=
  exist _ (existT _ (proj1_sig (snd x)) (__cast (proj2_sig (snd x)) (fst x)))
    (__lower (proj2_sig (snd x))).

Definition to_from_cast {n m : nat} (p : S n = m) (i : fin (S n))
  : _cast (__lower p) (__cast p i) = i.
Proof.
  apply equate_fin. reflexivity.
Qed.

Definition from_to_cast {n m : nat} (p : n = pred m) (i : fin m)
  : __cast (_uplift p i) (_cast p i) = i.
Proof.
  apply equate_fin; reflexivity.
Qed.

Lemma to_from_deriv_series {F : species} {n : nat} (x : deriv_series F n)
  : to_deriv_series (from_deriv_series x) = x.
Proof.
  unfold to_deriv_series.
  destruct x as [ ? [] ]; cbn.
  rewrite to_from_cast.
  f_equal.
  f_equal.
  apply UIP_nat.
Qed.

Lemma from_to_deriv_series {F : species} {n : nat} (x : sized_point F n)
  : from_deriv_series (to_deriv_series x) = x.
Proof.
  unfold from_deriv_series.
  destruct x as [ [] ? ]; cbn.
  apply eq_exist_irrel; [ intros; apply UIP_nat | cbn ].
  f_equal. apply from_to_cast.
Qed.

Definition is_bijection_deriv_series {F : species} {n : nat}
  : is_bijection (@from_deriv_series F n) (@to_deriv_series F n) :=
  {| fg := from_to_deriv_series ; gf := to_from_deriv_series |}.

Definition bij_deriv_series {F : species} {n : nat}
  : deriv_series F n <--> sized_point F n :=
  {| bij_is_bijection := is_bijection_deriv_series |}.

Definition sized_point_is_finite {F : species} (n : nat) : is_finite (sized_point F n) :=
  is_finite_bij bij_deriv_series (deriv_series F n).

Definition point_species (F : species) : species :=
  {| sp_type := { a : F & fin (sp_size F a) }
   ; sp_size := fun x => pred (sp_size F (projT1 x))
   ; sp_finite := sized_point_is_finite
  |}.

(** * List species *)

Definition unique_sigma_eq {A : Type} {a : A} :
  forall (x : {a' : A | a = a'}), exist _ a eq_refl = x := fun x =>
  match x with
  | exist _ _ r =>
    match r with
    | eq_refl => eq_refl
    end
  end.

Definition list_species : species :=
  {| sp_type := nat
   ; sp_size := fun n => n
   ; sp_finite := fun n => singleton_is_finite unique_sigma_eq |}.
