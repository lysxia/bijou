From Coq Require Import Arith Vector CMorphisms ssreflect.

From Bijou Require Import Bijection Fin Finite.

Definition powerset_ : nat -> Set := Vector.t bool.

Definition singleton_powerset_0 : forall x : powerset_ 0, Vector.nil = x :=
  Vector.case0 _ eq_refl.

Fixpoint Bij_power {n} : powerset_ n <--> fin (2 ^ n) :=
  match n with
  | O => Bij_singleton singleton_powerset_0
  | S n => symmetry Bij_Vector_cons >>> Bij_prod Bij_bool Bij_power >>> Bij_Fin_prod
  end%bij.

Definition is_finite_powerset n : is_finite (powerset_ n) :=
  {| enum := Bij_power |}.

Definition powerset (n : nat) : finite_type :=
  {| ft_is_finite := is_finite_powerset n |}.

Fixpoint size_powerset {n : nat} (sv : powerset n) : nat :=
  match sv with
  | [] => 0
  | true :: sv => S (size_powerset sv)
  | false :: sv => size_powerset sv
  end%vector.

Lemma bound_size_powerset {n} (sv : powerset n) : size_powerset sv <= n.
Proof.
  induction sv as [ | b n sv]; [ | destruct b]; cbn; auto using le_n_S.
Qed.

Definition F_castS {n : nat} (i : fin n) : fin (S n) :=
  exist _ (proj1_sig i) (Nat.lt_trans _ _ _ (proj2_sig i) (Nat.lt_succ_diag_r _)).

Fixpoint size_powerset_fin {n : nat} (sv : powerset n) : fin (S n) :=
  match sv with
  | [] => FO
  | true :: sv => FS (size_powerset_fin sv)
  | false :: sv => F_castS (size_powerset_fin sv)
  end%vector.

Definition co_powerset {n : nat} : powerset n -> powerset n :=
  Vector.map negb.

Lemma size_co_powerset {n} (sv : powerset n)
  : size_powerset (co_powerset sv) = n - size_powerset sv.
Proof.
  induction sv as [ | b n sv IH ]; cbn [co_powerset Vector.map size_powerset];
    [ reflexivity | destruct b; cbn [negb co_powerset]].
  - cbn [minus]; assumption.
  - rewrite -> Nat.sub_succ_l by apply bound_size_powerset.
    f_equal; assumption.
Qed.

Module Powerset_S.

Definition to {n} (v : powerset (S n)) : powerset n + powerset n :=
  let v := Vector.uncons v in
  (if fst v then inl else inr) (snd v).

Definition from {n} (v : powerset n + powerset n) : powerset (S n) :=
  match v with
  | inl v => true :: v
  | inr v => false :: v
  end%vector.

Definition is_bijection {n} : is_bijection (@to n) from.
Proof.
  constructor.
  - intros []; reflexivity.
  - refine (fun v => Vector.caseS' v (fun _ => _) _).
    move=> []; reflexivity.
Qed.

End Powerset_S.

Definition powerset_S {n} : powerset (S n) <--> powerset n + powerset n :=
  {| bij_is_bijection := Powerset_S.is_bijection |}.

Lemma size_const_false n : size_powerset (Vector.const false n) = 0.
Proof. induction n; cbn; auto. Qed.
