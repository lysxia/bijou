From Coq Require Import Fin Vector Setoid CMorphisms.
From Equations Require Import Equations.
From Bijou Require Import Bijection Fin.

Set Implicit Arguments.
Set Primitive Projections.

(** * Definitions *)

(** ** Finiteness predicate *)

Record is_finite (A : Type) : Type :=
  { card : nat
  ; enum :> A <--> fin card }.

Notation "'#|' X '|'" := (card X).

Lemma card_unique {A} (F1 F2 : is_finite A) : card F1 = card F2.
Proof.
  apply Fin_injective.
  etransitivity; [ symmetry; apply (enum F1) | apply (enum F2) ].
Qed.

(** ** Finite types *)

Record finite_type : Type :=
  { ft_type :> Type
  ; ft_is_finite :> is_finite ft_type }.

Declare Scope finite_scope.
Delimit Scope finite_scope with finite.
Bind Scope finite_scope with finite_type.

Definition enum_ {A : finite_type} : A <--> fin #|A| := enum A.

Lemma card_eq' {A B} (FA : is_finite A) (FB : is_finite B) (b : A <--> B) : card FA = card FB.
Proof.
  apply Fin_injective.
  etransitivity; [ symmetry; apply FA | ].
  etransitivity; [ apply b | apply FB ].
Qed.

Lemma card_eq {A B : finite_type} : (A <--> B) -> #|A| = #|B|.
Proof. apply card_eq'. Defined.

Lemma bij_card {A : finite_type} {n : nat} : #|A| = n -> (A <--> fin n).
Proof.
  intros <-; apply enum.
Defined.

Definition is_finite_bij {A B : Type} (bij : A <--> B) (FA : is_finite A) : is_finite B :=
  {| enum := Bij_dual bij >>> enum FA |}.

(** * Standard finite types *)

(** ** Empty *)

Definition absurd_is_finite {A : Type} (NA : A -> False) : is_finite A :=
  {| card := 0
   ; enum := Bij_0 NA |}.

Definition absurd_finite {A : Type} (NA : A -> False) : finite_type :=
  {| ft_is_finite := absurd_is_finite NA |}.

Definition False_is_finite : is_finite False := absurd_is_finite (fun x => x).
Definition False_finite : finite_type := {| ft_is_finite := False_is_finite |}.

(** ** Unit *)

Definition singleton_is_finite {A : Type} (a : A) (H : forall a', a = a') : is_finite A :=
  {| card := 1
   ; enum := Bij_singleton H |}.

Definition singleton_finite {A : Type} (a : A) (H : forall a', a = a') : finite_type :=
  {| ft_is_finite := singleton_is_finite H |}.

Definition True_is_finite : is_finite True :=
  singleton_finite (a := I) (fun z => match z with I => eq_refl end).
Definition True_finite : finite_type := {| ft_is_finite := True_is_finite |}.

(** ** Sums *)

Definition sum_is_finite {A B : Type} (FA : is_finite A) (FB : is_finite B)
  : is_finite (A + B) :=
  {| card := #|FA| + #|FB|
  ;  enum := transitivity (Bij_sum FA FB) Bij_Fin_sum |}. 

Definition sum_finite (A B : finite_type) : finite_type :=
  {| ft_is_finite := sum_is_finite A B |}.

Infix "+" := sum_finite : finite_scope.

Definition card_sum' {A B} (FA : is_finite A) (FB : is_finite B)
  : #|sum_is_finite FA FB| = #|FA| + #|FB|
  := eq_refl.

Definition card_sum (A B : finite_type) : #|(A + B)%finite| = #|A| + #|B|
  := eq_refl.

(** ** Products *)

Definition prod_is_finite {A B : Type} (FA : is_finite A) (FB : is_finite B)
  : is_finite (A * B) :=
  {| card := #|FA| * #|FB|
  ;  enum := transitivity (Bij_prod FA FB) Bij_Fin_prod |}.

Definition prod_finite (A B : finite_type) : finite_type :=
  {| ft_is_finite := prod_is_finite A B |}.

Infix "*" := prod_finite : finite_scope.

Definition card_prod {A B : finite_type} : #|(A * B)%finite| = #|A| * #|B|
  := eq_refl.

(** ** Fin *)

(* The canonical finite types: [enum] is the identity bijection. *)

Definition fin_is_finite (n : nat) : is_finite (fin n) :=
  {| card := n
   ; enum := Bij_id |}.

Definition finite_fin (n : nat) : finite_type :=
  {| ft_is_finite := fin_is_finite n |}.
