Set Implicit Arguments.

Require Import Equations.Prop.Equations.

From Coq Require Import Fin Vector Arith.

Module Import Pigeonhole.

Equations shift (n : nat) (i : Fin.t (S n)) (j : Fin.t n) : Fin.t (S n) :=
shift (FS i) (FS j) := FS (shift i j) ;
shift (FS _) F1 := F1 ;
shift F1 j := FS j
.

Equations Fin1_singleton (x y : Fin.t 1) : x = y :=
Fin1_singleton F1 F1 := eq_refl
.

Lemma shift_inv n (i : Fin.t (S n))
  : forall (k : Fin.t (S n)), i = k \/ exists j, shift i j = k.
Proof.
  induction n as [ | n IH ]; intros k.
  - left; apply Fin1_singleton.
  - apply (Fin.caseS' i).
    + apply (Fin.caseS' k).
      * left; reflexivity.
      * intros k'; right; exists k'. simp shift. reflexivity.
    + intros i'; apply (Fin.caseS' k).
      * right. exists F1. simp shift. reflexivity.
      * intros k'. specialize (IH i' k').
        destruct IH as [-> | [j IH]].
        { left; reflexivity. }
        { right; exists (FS j); simp shift. f_equal; assumption. }
Qed.

Lemma pigeonhole_f_
  : forall (holes pigeons : nat) (f : Fin.t pigeons -> Fin.t (S holes)) (n : Fin.t (S holes)),
      (exists i, f i = n) \/ (exists g, forall i, f i = shift n (g i)).
Proof.
  intros holes; induction pigeons.
  - intros; right. exists (Fin.case0 _); apply Fin.case0.
  - intros f n.
    destruct (shift_inv n (f F1)) as [Hj | [j Hj]].
    + left; eauto.
    + specialize (IHpigeons (fun i => f (FS i)) n).
      destruct IHpigeons as [[i Hi]|[g Hg]].
      * left; exists (FS i); assumption.
      * right. exists (fun x => Fin.caseS' x _ j g).
        intros i; apply (Fin.caseS' i); cbn; auto.
Qed.

Lemma pigeonhole_f
  : forall (holes pigeons : nat) (f : Fin.t (S pigeons) -> Fin.t (S holes)),
      (exists i, f (FS i) = f F1) \/ (exists g, forall i, f (FS i) = shift (f F1) (g i)).
Proof.
  intros holes pigeons f; apply pigeonhole_f_.
Qed.

End Pigeonhole.

Theorem pigeonhole_principle
  : forall (holes pigeons : nat), holes < pigeons ->
      forall (f : Fin.t pigeons -> Fin.t holes),
        exists i j, i <> j /\ f i = f j.
Proof.
  induction holes as [ | holes IH ]; intros pigeons INEQ f.
  { exfalso. inversion INEQ; subst.
    1,2: apply (Fin.case0 (fun _ => False) (f F1)). }
  assert (pigeons = S (pred pigeons)).
  { inversion INEQ; reflexivity. }
  revert INEQ f. rewrite H; clear H; intros.
  apply Nat.succ_lt_mono in INEQ.
  assert (PH := pigeonhole_f f).
  destruct PH as [[i Hi] | [g Hg]].
  - exists F1, (FS i). split; [ discriminate | auto ].
  - specialize (IH _ INEQ g).
    destruct IH as [i [j [Eij Egij]]].
    exists (FS i), (FS j).
    split; [ intros HFS; apply FS_inj in HFS; auto | rewrite 2 Hg; f_equal; assumption ].
Qed.
