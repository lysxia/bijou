From Coq Require Import Bool Arith List ssreflect ssrbool.
From Equations Require Import Equations.
From Bijou Require Import Fin Bijection Finite Setof.

Set Implicit Arguments.

Declare Scope eqdec_scope.
#[global] Open Scope eqdec_scope.

Infix "=?" := eq_dec : eqdec_scope.

#[global]
Instance EqDec_fin (n : nat) : EqDec (fin n) := fin_eq_dec.

Lemma injective_retract {A B} {f : A -> B} {g : B -> A} :
  (forall x, g (f x) = x) ->
  forall x y, f x = f y -> x = y.
Proof.
  intros H x y Hxy. apply (f_equal g) in Hxy. rewrite H H in Hxy. exact Hxy.
Qed.

#[global]
Instance EqDec_finite_type (A : finite_type) : EqDec A :=
  fun x y =>
    match eq_dec (bij_to (enum A) x) (bij_to (enum A) y) with
    | left H => left (injective_retract (gf (enum A)) _ _ H)
    | right H => right (fun Z => H (f_equal _ Z))
    end.

Definition Inb {A} `{EqDec A} (x : A) : list A -> bool :=
  existsb (fun y => eq_dec x y).

Lemma eqP {A} `{EqDec A} (x y : A) : reflect (x = y) (eq_dec x y).
Proof.
  destruct eq_dec; constructor; auto.
Defined.

Lemma InP {A} `{EqDec A} (x : A) (xs : list A) : reflect (In x xs) (Inb x xs).
Proof.
  induction xs; cbn.
  - constructor; auto.
  - apply orPP; [ | assumption ].
    apply (equivP (eqP _ _)). split; apply eq_sym.
Qed.

Fixpoint fin_forall {n : nat} : (fin n -> bool) -> bool :=
  match n with
  | O => fun _ => true
  | S n => fun f => f FO && fin_forall (fun z => f (Fin.FS z))
  end.

Lemma fin_forallP {n : nat} (f : fin n -> bool) : reflect (forall z, f z) (fin_forall f).
Proof.
  apply Bool.iff_reflect. induction n; cbn.
  - split; [ reflexivity | intros ? z; apply fin_case0; auto ].
  - rewrite Bool.andb_true_iff. rewrite <- (IHn (fun z => f (Fin.FS z))).
    split; [ split; intros; apply H | ].
    intros []. apply Fin_caseS; auto.
Qed.

Definition finite_forall {A : finite_type} (f : A -> bool) : bool :=
  fin_forall (fun i => f (bij_from A i)).

Lemma finite_forallP {A : finite_type} (f : A -> bool) : reflect (forall x, f x) (finite_forall f).
Proof.
  apply (equivP (fin_forallP _)).
  split; intros H j.
  - rewrite <- (gf A j). apply H.
  - apply H.
Qed.

Definition setof_forall {A} (X : finite_set_of A) (f : forall x : A, X x -> bool) : bool :=
  fin_forall (fun i => f _ (proj2_sig (bij_from X i))).

Lemma setof_forallP {A} (X : finite_set_of A) (f : forall x : A, X x -> bool)
  : reflect (forall x (Hx : X x), f x Hx) (setof_forall X f).
Proof.
  apply (equivP (fin_forallP _)).
  split; intros H j.
  - intros Hj. change ((fun w => f (proj1_sig w) (proj2_sig w)) (exist (fun j => X j) j Hj)).
    rewrite <- (gf X _). apply H.
  - apply H.
Qed.

Definition EqDecPoint_setof_elem {A} (X : finite_set_of A) (x : A) (Hx : X x) : EqDecPoint A x.
Proof.
  red. intros y. destruct (X y) eqn:Hy.
  - destruct (eq_dec (bij_to X (exist _ x Hx)) (bij_to X (exist _ y Hy))) as [ | Hj ].
    + left.
      apply (f_equal (bij_from X)) in e. do 2 rewrite (gf X) in e.
      apply (f_equal (@proj1_sig _ _) e).
    + right. intros <-. apply Hj. f_equal. apply Setof.ssig_eq_inv; reflexivity.
  - right. intros <-. congruence.
Qed.
