(** * Sets as predicates over a universe *)

(** This representation supports unions and intersections. *)

(** One of the main basic results this lets us formulate
    is inclusion-exclusion ([card_overlap]). *)

From Coq Require Import Arith ssreflect ssrbool Setoid.
From Equations Require Import Equations.
From Bijou Require Import Util Bijection Fin Finite.

Set Primitive Projections.
Set Implicit Arguments.

Import EqNotations.
#[local] Open Scope bool_scope.

Declare Scope setof_scope.
Delimit Scope setof_scope with setof.
#[local] Open Scope setof_scope.

Declare Scope fsetof_scope.
Delimit Scope fsetof_scope with fsetof.
#[local] Open Scope fsetof_scope.

Module Import Setof.

(** * Definition *)

(** Sets encoded as predicates *)
Definition t (A : Type) : Type := A -> bool.

#[local] Notation set_of := t.

Bind Scope setof_scope with t.

(** ** Equivalences *)

(** Extensional equivalence of predicates *)
Definition eq_setof {A : Type} (X Y : set_of A) : Prop :=
  forall a, X a = Y a.

Infix "==" := eq_setof (at level 70) : setof_scope.

(** Inclusion *)
Definition subset_setof {A : Type} (X Y : set_of A) : Prop :=
  forall a, X a -> Y a.

Definition ssig {A : Type} (X : set_of A) : Type := { a : A | X a }.

#[local]
Coercion ssig : set_of >-> Sortclass.

Section Setof.

Context {A : Type}.
Implicit Types X Y Z : set_of A.

Lemma ssig_eq_inv {X} (x y : ssig X) : proj1_sig x = proj1_sig y -> x = y.
Proof.
  apply eq_sig_hprop.
  intros ?; apply EqDec.eq_proofs_unicity.
Qed.

Lemma eta_ssig {P : set_of A} (x : ssig P) (H : P _) : exist P (proj1_sig x) H = x.
Proof.
  apply ssig_eq_inv; reflexivity.
Qed.

Definition is_empty X : Prop := forall x, ~ X x.

Definition singleton (x : A) `{EqDecPoint A x} : set_of A := fun y => eq_dec_point y.

Arguments singleton : clear implicits.
Arguments singleton x {_}.

Definition top : set_of A := fun _ => true.
Definition bot : set_of A := fun _ => false.
Definition union X Y : set_of A := fun a => X a || Y a.
Definition intersection X Y : set_of A := fun a => X a && Y a.
Definition difference X Y : set_of A := fun a => X a && negb (Y a).
Definition complement X : set_of A := fun a => negb (X a).

Definition disjoint X Y : Prop := forall a, X a <> Y a.

Section Bij_eq_setof.

Context {X Y} (EXY : (X == Y)).

Definition eq_setof__to_2 (x : ssig X) : Y (proj1_sig x) = true :=
  eq_trans (eq_sym (EXY (proj1_sig x))) (proj2_sig x).

Definition eq_setof__from_2 (y : ssig Y) : X (proj1_sig y) = true :=
  eq_trans (EXY (proj1_sig y)) (proj2_sig y).

Lemma is_bijection_eq_setof
  : @is_bijection (ssig X) (ssig Y)
      (fun (x : ssig X) => exist _ _ (eq_setof__to_2 x))
      (fun (y : ssig Y) => exist _ _ (eq_setof__from_2 y)).
Proof.
  constructor; intros; apply ssig_eq_inv; cbn; reflexivity.
Defined.

Definition Bij_eq_setof : ssig X <--> ssig Y :=
  {| bij_is_bijection := is_bijection_eq_setof |}.

End Bij_eq_setof.

Lemma is_bijection_top
  : @is_bijection A (ssig top) (fun a => exist _ a eq_refl) (fun x => proj1_sig x).
Proof.
  constructor; intros; by [ apply ssig_eq_inv | ].
Qed.

Definition Bij_top : A <--> ssig top :=
  {| bij_is_bijection := is_bijection_top |}.

Lemma is_bijection_bot
  : @is_bijection False (ssig bot)
      (fun a => match a with end)
      (fun x => match Bool.diff_false_true (proj2_sig x) with end).
Proof.
  constructor; intros; by destruct _.
Qed.

Section Bij_disjoint.

Context {X Y} (DXY : disjoint X Y).

(* Disjoint intersection *)

Lemma disjoint_intersection_contra (x : ssig (intersection X Y)) : False.
Proof.
  destruct x as [a H]. unfold intersection in H.
  specialize (@DXY a).
  destruct (X a), (Y a); cbn in H; discriminate + contradiction.
Qed.

Lemma is_bijection_disjoint_intersection
  : @is_bijection (ssig (intersection X Y)) False
      disjoint_intersection_contra
      (fun x : False => match x with end).
Proof.
  constructor; intros; destruct (_ : False).
Qed.

Definition Bij_disjoint_intersection : ssig (intersection X Y) <--> False :=
  {| bij_is_bijection := is_bijection_disjoint_intersection |}.

(* Disjoint union *)

Lemma disjoint_union__aux {a : A} : X a = false -> Y a = true.
Proof.
  intros H; specialize (@DXY a). destruct (Y a); [ reflexivity | contradiction ].
Qed.

Lemma disjoint_union__aux_2 {a : A} : Y a = true -> X a = false.
Proof.
  intros H; specialize (@DXY a). destruct (X a); [ contradiction DXY; auto | reflexivity ].
Qed.

Equations disjoint_union__to_ {a : A} (b : bool) : X a = b -> ssig X + ssig Y :=
  disjoint_union__to_ (b := true) H := inl (exist _ a H) ;
  disjoint_union__to_ (b := false) H := inr (exist _ a (disjoint_union__aux H)).

Definition disjoint_union__to (x : ssig (union X Y)) : ssig X + ssig Y :=
  disjoint_union__to_ (b := X (proj1_sig x)) eq_refl.

Definition disjoint_union__from_inl_2 (x : ssig X) : X (proj1_sig x) || Y (proj1_sig x) = true :=
  match eq_sym (proj2_sig x) with
  | eq_refl => eq_refl
  end.

Definition disjoint_union__from_inr_2 (x : ssig Y) : X (proj1_sig x) || Y (proj1_sig x) = true :=
  match X (proj1_sig x) with
  | true => eq_refl
  | false => proj2_sig x
  end.

Definition disjoint_union__from (xy : ssig X + ssig Y) : ssig (union X Y) :=
  match xy with
  | inl x => exist _ _ (disjoint_union__from_inl_2 x)
  | inr y => exist _ _ (disjoint_union__from_inr_2 y)
  end.

Lemma is_bijection_disjoint_union
  : @is_bijection (ssig (union X Y)) (ssig X + ssig Y)
      disjoint_union__to disjoint_union__from.
Proof.
  unfold disjoint_union__to. constructor.
  - intros [x | y]; cbn.
    + generalize (@eq_refl bool (X (proj1_sig x))).
      pose (b := X (proj1_sig x)). fold b; unfold b at 2 3. rewrite (proj2_sig x).
      intros e; simp disjoint_union__to_.
      f_equal; apply ssig_eq_inv; reflexivity.
    + generalize (@eq_refl bool (X (proj1_sig y))).
      pose (b := X (proj1_sig y)). fold b; unfold b at 2 3.
      rewrite (disjoint_union__aux_2 (proj2_sig y)).
      intros e; simp disjoint_union__to_.
      f_equal; apply ssig_eq_inv; reflexivity.
  - intros x; cbn. 
    generalize (@eq_refl bool (X (proj1_sig x))).
    pose (b := X (proj1_sig x)). fold b; unfold b at 1.
    destruct b; intros e; simp disjoint_union__to_; cbn; apply ssig_eq_inv; reflexivity.
Qed.

Definition Bij_disjoint_union : ssig (union X Y) <--> ssig X + ssig Y :=
  {| bij_is_bijection := is_bijection_disjoint_union |}.

End Bij_disjoint.

Lemma complement_union {X Y}
  : complement (union X Y) == intersection (complement X) (complement Y).
Proof.
  exact (fun _ => Bool.negb_orb _ _).
Defined.

Lemma complement_intersection {X Y}
  : complement (intersection X Y) == union (complement X) (complement Y).
Proof.
  exact (fun _ => Bool.negb_andb _ _).
Defined.

End Setof.

Arguments singleton : clear implicits.
Arguments singleton {A} x {_}.

Record is_setof_bijection {A B} (X : set_of A) (Y : set_of B)
    (f : A -> option B) (g : B -> option A) : Prop :=
  { setof_f : forall a : A, X a -> prop_option (f a) Y
  ; setof_g : forall b : B, Y b -> prop_option (g b) X
  ; setof_fg : forall a : A, X a -> (f >=> g)%option a = Some a
  ; setof_gf : forall b : B, Y b -> (g >=> f)%option b = Some b
  }.

Record SetofBijection {A B} (X : set_of A) (Y : set_of B) : Type :=
  { sbij_to : A -> option B
  ; sbij_from : B -> option A
  ; sbij_is_setof_bijection :> is_setof_bijection X Y sbij_to sbij_from
  }.

Module Notations.
Infix "-" := difference : setof_scope.
Infix "∪" := union (at level 40) : setof_scope.
Infix "∩" := intersection (at level 40) : setof_scope.
Infix "<-->" := SetofBijection : setof_scope.
End Notations.

End Setof.

Export Setof.Notations.

Notation set_of := Setof.t.

Bind Scope setof_scope with Setof.t.

Coercion ssig : set_of >-> Sortclass.

Record finite_set_of (A : Type) :=
  { fs_setof :> set_of A
  ; fs_is_finite :> is_finite (ssig fs_setof) }.

Definition elem_finite_set_of {A} (X : finite_set_of A) : A -> bool :=
  fs_setof X.

Coercion elem_finite_set_of : finite_set_of >-> Funclass.

Lemma is_left_proof {P : Prop} (h : {P} + {~P}) : P -> is_left h.
Proof.
  destruct h; auto. 
Defined.

Lemma is_left_inv {P : Prop} (h : {P} + {~P}) : is_left h -> P.
Proof. destruct h; cbn; auto; discriminate. Defined.

Definition is_bijection_singleton {A} (x : A) `{EqDecPoint A x}
  : is_bijection
      (fun x : ssig (singleton x) => @FO 0)
      (fun _ => exist _ x (is_left_proof _ eq_refl)).
Proof.
  constructor; cbn.
  - intros b; symmetry; apply singleton_Fin_1.
  - intros a; apply ssig_eq_inv; cbn. apply (is_left_inv _ (proj2_sig a)).
Qed.

Definition Bij_singleton' {A} (x : A) `{EqDecPoint A x}
  : singleton x <--> fin 1 :=
  {| bij_is_bijection := is_bijection_singleton |}.

Record section {A : Type} (X : set_of A) (B : Type) : Type :=
  { sec_to : A -> option B
  ; sec_from : B -> option A
  ; sec_inj :> forall a, X a -> (sec_to >=> sec_from)%option a = Some a
  }.

Definition finite_section {A : Type} (X : set_of A) (n : nat) : Type :=
  section X (fin n).

Module FiniteSection.

Section FR.

Context {A : Type}.
Implicit Types X : set_of A.
Generalizable Variables X n.

Definition paired `(R : finite_section X n) (i : fin n) : bool :=
  bool_option (sec_from R i) X
  && bool_option ((sec_from R >=> sec_to R)%option i) (fin_eqb i).

Lemma fin_eq_eqb {n : nat} {i : fin n} : fin_eqb i i = true.
Proof. unfold fin_eqb; apply Nat.eqb_eq. reflexivity. Qed.

Lemma fin_eqb_eq {n : nat} {i j : fin n} : fin_eqb i j = true -> i = j.
Proof. intros; apply equate_fin. apply Nat.eqb_eq. auto. Qed.

Definition sec_f `(R : finite_section X n) (a : A)
  : X a -> bool_option (sec_to R a) (paired R).
Proof.
  move => H. move: (sec_inj R a H). unfold paired. rewrite /kleisli_option.
  destruct (sec_to R a) as [ i | ] eqn:Ei => /=; [ | discriminate].
  move => -> /=.
  apply /andP; split; [ assumption | ].
  rewrite Ei => /=. by apply fin_eq_eqb.
Qed.

Definition finite_section_empty `(R : finite_section X 0) : ssig X -> False := fun x =>
  match sec_to R (proj1_sig x) as oi return bool_option oi _ -> _ with
  | None => fun H => ltac:(discriminate H)
  | Some i => fun _ => fin_case0 _ i
  end (sec_f R _ (proj2_sig x)).

Lemma finite_section_empty_finite `(R : finite_section X 0) : is_finite (ssig X).
Proof.
  exists 0. apply Bij_0, finite_section_empty, R.
Defined.

Definition is_FS {n} (i : fin n) : bool :=
  match proj1_sig i with O => false | S _ => true end.

Lemma is_FS_lemma {n} (i : fin (S n)) : is_FS i -> Nat.pred (proj1_sig i) < n.
Proof.
  destruct i; cbn. destruct x; cbn.
  - discriminate.
  - intros _. apply Nat.succ_lt_mono. auto.
Qed.

Definition predFS {n} (i : fin (S n)) : is_FS i -> fin n := fun H =>
  exist _ (Nat.pred (proj1_sig i)) (is_FS_lemma i H).

Definition predFS_eq {n} (i : fin (S n))
  : forall (H : is_FS i), i = FS (predFS i H).
Proof.
  intros H; apply equate_fin; cbn. destruct i as [[] ?]; cbn in *.
  - discriminate.
  - reflexivity.
Qed.

Section PredFinset.

Context `(R : finite_section X (S n)).

Definition pred_setof : set_of A := fun a =>
  X a && bool_option (sec_to R a) is_FS.

Definition pred_to (a : A) : option (fin n) :=
  let? i := sec_to R a in
  match @idP (is_FS i) with
  | Bool.ReflectF _ _ => None
  | ReflectT _ e => Some (predFS i e)
  end.

Definition pred_from (i : fin n) : option A :=
  sec_from R (FS i).

Lemma pred_inj
  : forall a, pred_setof a -> (pred_to >=> pred_from)%option a = Some a.
Proof.
  move=> a /andP [Ha /prop_optionP Hfa].
  move: (sec_inj R a Ha).
  rewrite /pred_to /pred_from /kleisli_option.
  elim/prop_option_ind: Hfa => //= i Hi.
  intros.
  destruct idP; cbn; [ | contradiction ].
  by rewrite <- (predFS_eq i _).
Qed.

Definition pred_finite_section : finite_section pred_setof n :=
  {| sec_inj := pred_inj |}.

Definition weaken_ (a : A) (H : pred_setof a) : X a :=
  proj1 (elimT andP H).

Definition weaken (x : ssig pred_setof) : ssig X :=
  exist _ (proj1_sig x) (weaken_ _ (proj2_sig x)).

Section SuccBijection.

Context (m : nat) (B : ssig pred_setof <--> fin m).

Section SuccBijection1.

Context (H0 : paired R FO).

Definition H01 : bool_option (sec_from R FO) X :=
  proj1 (elimT andP H0).

Definition H02 : (sec_from R >=> sec_to R)%option FO = Some FO.
Proof.
  assert (Y := proj2 (elimT andP H0)).
  destruct ((_ >=> _)%option _); cbn [bool_option] in Y.
  - apply fin_eqb_eq in Y. move: Y => <-; reflexivity.
  - discriminate.
Qed.

Equations split_pred_setof (a : A) (Ha : X a)
  : { X a && bool_option (sec_to R a) is_FS } + { sec_to R a = Some FO } :=
  | a, Ha with sec_f R a Ha :=
    | H with sec_to R a :=
      | None := _ ;
      | Some i with Fin_match i :=
        | F0' z := right _ ;
        | FS' z := left (introT andP (conj Ha _)).

Definition succ_to1 (x : ssig X) : fin (S m) :=
  match split_pred_setof (proj2_sig x) with
  | left H => FS (bij_to B (exist _ (proj1_sig x) H))
  | right _ => FO
  end.

Definition from0 : ssig X :=
  unwrap_prop_option (elimT prop_optionP H01).

Definition succ_from1 : fin (S m) -> ssig X := fun i =>
  match Fin_match i with
  | F0' _ => from0
  | @FS' _ _ i _ => weaken (bij_from B i)
  end.

Lemma succ_fg_1 : forall i, succ_to1 (succ_from1 i) = i.
Proof.
  intros i. unfold succ_from1. destruct Fin_match; unfold succ_to1; subst i.
  - elim: split_pred_setof => Hi; [ exfalso | reflexivity ].
    move: Hi => /andP [H1 H2].
    move: H02.
    rewrite /kleisli_option (unwrap_prop_option_eq (elimT prop_optionP H01)) => //= E.
    by rewrite /from0 E in H2.
  - elim split_pred_setof => Hi; [ f_equal | exfalso ].
    + cbn. rewrite eta_ssig. apply (fg B).
    + cbn in Hi.
      move: (proj2_sig (bij_from B z)).
      rewrite /pred_setof. move=> /andP [_].
      rewrite Hi //=.
Qed.

Lemma succ_gf_1 : forall x, succ_from1 (succ_to1 x) = x.
Proof.
  intros x. rewrite /succ_to1. elim: split_pred_setof => Hb; unfold succ_from1; cbn.
  - match goal with
    | [ |- context C [ (exist _ (proj1_sig ?w) _) ] ] =>
        replace (exist _ (proj1_sig w) _) with w;
        [ | apply equate_fin; reflexivity ]
    end.
    rewrite (gf B). apply ssig_eq_inv; reflexivity.
  - move: (sec_inj R (proj1_sig x) (proj2_sig x)) => H2.
    rewrite /kleisli_option Hb //= in H2.
    apply ssig_eq_inv; cbn. rewrite /from0 /unwrap_prop_option. generalize (prop_optionP H01).
    by rewrite H2.
Qed.

Definition succ_is_bijection_1 : is_bijection succ_to1 succ_from1 :=
  {| fg := succ_fg_1 ; gf := succ_gf_1 |}.

Definition is_finite_1 : is_finite (ssig X) :=
  {| enum := {| bij_is_bijection := succ_is_bijection_1 |} |}.

End SuccBijection1.

Section SuccBijection2.

Context (H0 : not (paired R FO)).

Equations lower_ (a : A) : X a -> bool_option (sec_to R a) is_FS :=
  | a, Ha with sec_f R a Ha :=
    | H with sec_to R a :=
      | Some i with Fin_match i :=
        | F0' _ := _
        | FS' _ := _ .

Definition lower (a : A) (Ha : X a) : pred_setof a :=
  introT andP (conj Ha (lower_ Ha)).

Definition succ_to2 (x : ssig X) : fin m :=
  bij_to B (exist _ (proj1_sig x) (lower (proj2_sig x))).

Definition succ_from2 (i : fin m) : ssig X :=
  weaken (bij_from B i).

Lemma succ_fg_2 : forall i, succ_to2 (succ_from2 i) = i.
Proof.
  intros i. by rewrite /succ_to2 /succ_from2 eta_ssig (fg B).
Qed.

Lemma succ_gf_2 : forall x, succ_from2 (succ_to2 x) = x.
Proof.
  intros x. rewrite /succ_to2 /succ_from2 (gf B). by apply ssig_eq_inv.
Qed.

Definition succ_is_bijection_2 : is_bijection succ_to2 succ_from2 :=
  {| fg := succ_fg_2 ; gf := succ_gf_2 |}.

Definition is_finite_2 : is_finite (ssig X) :=
  {| enum := {| bij_is_bijection := succ_is_bijection_2 |} |}.

End SuccBijection2.

End SuccBijection.

End PredFinset.

End FR.

End FiniteSection.

Theorem finite_section_finite {A} {n : nat}
 : forall {X : set_of A} (R : finite_section X n), is_finite (ssig X).
Proof.
  induction n as [ | n IH ]; intros X.
  - apply FiniteSection.finite_section_empty_finite.
  - intros R; specialize (IH _ (FiniteSection.pred_finite_section R)).
    elim IH=> m Hm.
    elim (@idP (FiniteSection.paired R FO)); move: Hm.
    + apply FiniteSection.is_finite_1.
    + apply FiniteSection.is_finite_2.
Defined.

Definition omap1 {A B C} (f : A -> option B) (g : B -> C) (x : A) : option C :=
  option_map g (f x).

Lemma is_section_compose_ {A B C} (X : set_of A)
    (f : A -> option B) (f' : B -> option A)
    (g : B -> C) (g' : C -> B)
  : (forall x, X x -> (f >=> f')%option x = Some x) ->
    (forall y, g' (g y) = y) ->
    (forall x, X x -> (omap1 f g >=> (fun z => f' (g' z)))%option x = Some x).
Proof.
  intros Hf Hg x Hx; unfold kleisli_option, omap1 in *.
  specialize (Hf x Hx).
  destruct (f x) as [ y | ] eqn:Ef; cbn in *; [ | discriminate ].
  rewrite Hg. auto.
Qed.

Definition bijection_finite_section {A} (X : set_of A) (B C : Type)
  : (B <--> C) -> section X B -> section X C :=
  fun bb ss => {| sec_inj := is_section_compose_ _ _ _ _ _ ss (gf bb) |}.

Definition section_finite_section {A} (X : set_of A) (B : Type)
  : forall FB : is_finite B, section X B -> finite_section X #|FB| :=
  fun FB => bijection_finite_section FB.

Section finite_union.

Context {A : Type} {X Y : set_of A} (fX : is_finite (ssig X)) (fY : is_finite (ssig Y)).

Definition sec_to_union (a : A) : option (X + Y) :=
  match @idP (X a), @idP (Y a) with
  | ReflectT _ H, _ => Some (inl (exist _ a H))
  | _, ReflectT _ H => Some (inr (exist _ a H))
  | _, _ => None
  end.

Definition sec_from_union (y : X + Y) : option A :=
  Some match y with
  | inl i | inr i => proj1_sig i
  end.

Definition sec_inj_union (a : A) (Ha : (X ∪ Y)%setof a) : (sec_to_union >=> sec_from_union)%option a = Some a.
Proof.
  rewrite /kleisli_option /sec_to_union /sec_from_union.
  elim (@idP (X a))=> HX /=; [ reflexivity |].
  elim (@idP (Y a)) => HY /=; [ reflexivity |].
  by move: Ha => /orP [].
Qed.

Definition section_union : section (X ∪ Y) (X + Y) :=
  {| sec_inj := sec_inj_union |}.

Definition finite_section_union : finite_section (X ∪ Y) (#|fX| + #|fY|) :=
  section_finite_section (sum_is_finite _ _) section_union.

Definition is_finite_union : is_finite (ssig (X ∪ Y)) :=
  finite_section_finite finite_section_union.

End finite_union.

Definition union {A} (X Y : finite_set_of A) : finite_set_of A :=
  {| fs_is_finite := is_finite_union X Y |}.

Section finite_intersection.

Context {A : Type} {X : set_of A} (fX : is_finite (sig X)) (Y : set_of A).

Definition sec_to_intersection (a : A) : option X :=
  match @idP (X a) with
  | ReflectT _ H => Some (exist _ a H)
  | ReflectF _ _ => None
  end.

Definition sec_from_intersection (i : X) : option A :=
  Some (proj1_sig i).

Definition sec_inj_intersection (a : A) (Ha : (X ∩ Y) a)
  : (sec_to_intersection >=> sec_from_intersection)%option a = Some a.
Proof.
  rewrite /kleisli_option /sec_to_intersection /sec_from_intersection.
  destruct idP; cbn; [ reflexivity | ].
  by move: Ha => /andP [].
Qed.

Definition section_intersection : section (X ∩ Y) X :=
  {| sec_inj := sec_inj_intersection |}.

Definition finite_section_intersection : finite_section (X ∩ Y) #|fX| :=
  section_finite_section _ section_intersection.

Definition is_finite_intersection : is_finite (X ∩ Y) :=
  finite_section_finite finite_section_intersection.

End finite_intersection.

Definition intersection {A} (X Y : finite_set_of A) : finite_set_of A :=
  {| fs_is_finite := is_finite_intersection X Y |}.

Section finite_difference.

Context {A : Type} {X : set_of A} (fX : is_finite (sig X)) (Y : set_of A).

Definition sec_to_difference (a : A) : option X :=
  match @idP (X a) with
  | ReflectT _ H => Some (exist _ a H)
  | ReflectF _ _ => None
  end.

Definition sec_from_difference (x : X) : option A :=
  Some (proj1_sig x).

Definition sec_inj_difference (a : A) (Ha : (X - Y)%setof a)
  : (sec_to_difference >=> sec_from_difference)%option a = Some a.
Proof.
  rewrite /kleisli_option /sec_to_difference.
  destruct idP; cbn; [ reflexivity | ].
  by move: Ha => /andP [].
Qed.

Definition section_difference : section (X - Y) X :=
  {| sec_inj := @sec_inj_difference |}.

Definition finite_section_difference : finite_section (X - Y) #|fX| :=
  section_finite_section _ section_difference.

Definition is_finite_difference : is_finite (X - Y) :=
  finite_section_finite finite_section_difference.

End finite_difference.

Definition difference {A} (X Y : finite_set_of A) : finite_set_of A :=
  {| fs_is_finite := is_finite_difference X Y |}.

Module FSetof.

Lemma is_bijection_top {A} : is_bijection (A := A) (B := Setof.top (A := A))
  (fun x => exist _ x eq_refl) (fun x => proj1_sig x).
Proof.
  constructor; [ intros; apply Setof.ssig_eq_inv; reflexivity | reflexivity ].
Qed.

Definition bij_top {A} : A <--> Setof.top (A := A) :=
  {| bij_is_bijection := is_bijection_top |}.

Definition is_finite_top {A} (H : is_finite A) : is_finite (Setof.top (A := A)) :=
  {| enum := Bij_dual bij_top >>> enum H |}.

Definition top {A : finite_type} : finite_set_of A := {| fs_is_finite := is_finite_top A |}.

Definition is_finite_singleton {A} (x : A) `{EqDecPoint A x} : is_finite (singleton x) :=
  {| enum := Bij_singleton' |}.

Definition singleton {A} (x : A) `{EqDecPoint A x} : finite_set_of A :=
  {| fs_is_finite := is_finite_singleton |}.

Arguments singleton : clear implicits.
Arguments singleton {A} x {_}.

Module Notations.
Infix "∪" := union (at level 40) : fsetof_scope.
Infix "∩" := intersection (at level 40) : fsetof_scope.
Infix "-" := difference : fsetof_scope.
End Notations.
End FSetof.

Export FSetof.Notations.

Module Import UnionOverlap.
Section Overlap.

Context {A} (X Y : set_of A).

Notation SUM := (ssig X + ssig Y)%type.
Notation DUP := (ssig (X ∪ Y)%setof + ssig (X ∩ Y)%setof)%type.

Definition overlap_to (xy : SUM) : DUP :=
  match xy with
  | inl x => inl (exist _ (proj1_sig x) (introT orP (or_introl (proj2_sig x))))
  | inr y =>
    match @idP (X (proj1_sig y)) with
    | ReflectT _ H => inr (exist _ (proj1_sig y) (introT andP (conj H (proj2_sig y))))
    | ReflectF _ _ => inl (exist _ (proj1_sig y) (introT orP (or_intror (proj2_sig y))))
    end
  end.

Lemma or_not_l {a b : bool} : ~ a -> a || b -> b.
Proof.
  destruct a; auto.
Defined.

Definition overlap_from (ui : DUP) : SUM :=
  match ui with
  | inl u =>
    match @idP (X (proj1_sig u)) with
    | ReflectT _ H => inl (exist _ (proj1_sig u) H)
    | ReflectF _ H => inr (exist _ (proj1_sig u) (or_not_l H (proj2_sig u)))
    end
  | inr i => inr (exist _ (proj1_sig i) (proj2 (elimT andP (proj2_sig i))))
  end.

Ltac dmatch_ t := lazymatch t with (match ?x with _ => _ end) => dmatch_ x | _ => destruct t end.

Ltac dmatch := lazymatch goal with [ |- context [ match ?x with _ => _ end ] ] => dmatch_ x end.

Lemma overlap_is_bijection : is_bijection overlap_to overlap_from.
Proof.
  constructor; intros x; rewrite /overlap_to /overlap_from.
  - repeat dmatch; cbn; try (contradiction + f_equal; apply ssig_eq_inv; reflexivity).
    refine (_  (proj2_sig s)) => /andP []; contradiction.
  - repeat dmatch; cbn; try (contradiction + f_equal; apply ssig_eq_inv; reflexivity).
    move: (proj2_sig s); contradiction.
Qed.

Definition overlap : SUM <--> DUP :=
  {| bij_is_bijection := overlap_is_bijection |}.

End Overlap.
End UnionOverlap.

Definition finite_setof_to_set {A} (X : finite_set_of A) : finite_type :=
  {| Finite.ft_type := X ; Finite.ft_is_finite := X |}.

#[local] Notation toset := finite_setof_to_set.

Coercion toset : finite_set_of >-> finite_type.

Lemma card_overlap {A} (X Y : finite_set_of A)
  : #|X| + #|Y| = #|X ∪ Y| + #|X ∩ Y|.
Proof.
  rewrite <- 2 card_sum'. apply card_eq', overlap.
Qed.

Module Import DifferenceOverlap.
Section Overlap.

Context {A} (X Y : set_of A).

Notation DUP := ((X - Y)%setof + (X ∩ Y)%setof)%type.

Lemma not_negb (b : bool) : ~ b -> ~~ b.
Proof.
  apply Bool.eq_true_not_negb.
Defined.

Definition overlap_to (x : X) : DUP :=
  match @idP (Y (proj1_sig x)) with
  | ReflectT _ H => inr (exist _ (proj1_sig x) (introT andP (conj (proj2_sig x) H)))
  | ReflectF _ H => inl (exist _ (proj1_sig x) (introT andP (conj (proj2_sig x) (not_negb H))))
  end.

Lemma or_not_l {a b : bool} : ~ a -> a || b -> b.
Proof.
  destruct a; auto.
Defined.

Definition overlap_from (ui : DUP) : X :=
  match ui with
  | inl u | inr u => exist _ (proj1_sig u) (proj1 (elimT andP (proj2_sig u)))
  end.

Ltac dmatch_ t := lazymatch t with (match ?x with _ => _ end) => dmatch_ x | _ => destruct t end.

Ltac dmatch := lazymatch goal with [ |- context [ match ?x with _ => _ end ] ] => dmatch_ x end.

Lemma overlap_is_bijection : is_bijection overlap_to overlap_from.
Proof.
  constructor; intros x; rewrite /overlap_to /overlap_from.
  - repeat dmatch; cbn in *; try (contradiction + f_equal; apply ssig_eq_inv; reflexivity).
    + refine (_  (proj2_sig s)) => /andP [_ /negP]; contradiction.
    + contradiction n; move: (proj2_sig s) => /andP [_]; auto.
  - repeat dmatch; cbn; try (contradiction + f_equal; apply ssig_eq_inv; reflexivity).
Qed.

Definition overlap : X <--> DUP :=
  {| bij_is_bijection := overlap_is_bijection |}.

End Overlap.
End DifferenceOverlap.

Lemma card_difference {A} (X Y : finite_set_of A)
  : #|X| = #|(X - Y)%fsetof| + #|X ∩ Y|.
Proof.
  rewrite <- card_sum'. apply card_eq', overlap.
Qed.

Definition eq_to {A} (X Y : set_of A) (H : X == Y) : X -> Y :=
  fun x => exist _ (proj1_sig x) (Logic.transport _ (H (proj1_sig x)) (proj2_sig x)).

Definition is_bijection_eq {A} (X Y : set_of A) (H : X == Y)
  : is_bijection (eq_to H) (eq_to (fun x => sym_eq (H x))).
Proof.
  constructor; intros ?; apply ssig_eq_inv; reflexivity.
Defined.

Definition Bij_eq_setof {A} (X Y : set_of A)
  : X == Y -> X <--> Y
  := fun H => {| bij_is_bijection := is_bijection_eq H |}.

Lemma intersect_subset {A} (X Y : finite_set_of A)
  : subset_setof Y X -> #|X ∩ Y| = #|Y|.
Proof.
  intros H. apply card_eq', Bij_eq_setof.
  intros x; unfold intersection, Setof.Setof.intersection; cbn.
  specialize (H x). destruct (@fs_setof _ Y x). rewrite (H eq_refl); reflexivity.
  apply Bool.andb_false_r.
Qed.

Lemma singleton_subset {A} (x : A) `{EqDecPoint A x} (X : set_of A)
  : X x <-> subset_setof (singleton x) X.
Proof.
  split.
  - move=> Hx y /is_left_inv <-. auto.
  - move=> HX; apply HX. apply is_left_proof. reflexivity.
Qed.

Theorem card_is_empty {A : Type} {X : finite_set_of A} : Setof.is_empty X -> #|X| = 0.
Proof.
  intros H. apply (card_eq (A := X) (B := False_finite)).
  exact (Bij_False (fun x => H _ (proj2_sig x))).
Qed.

Theorem is_empty_0 {A : Type} {X : finite_set_of A} : #|X| = 0 -> Setof.is_empty X.
Proof.
  intros H x Hx. apply (bij_card (A := X)) in H.
  move: (bij_to H (exist _ x Hx)). apply fin_case0.
Qed.

Theorem card_pred {A : Type} {X : finite_set_of A} (x : A) `{EqDecPoint A x} (Xx : X x)
  : #|X| = S (#|(X - FSetof.singleton x)%fsetof|).
Proof.
  rewrite <- Nat.add_1_r.
  change 1 with #|FSetof.singleton x|.
  rewrite (card_difference _ (FSetof.singleton x)).
  f_equal. apply intersect_subset, singleton_subset. auto.
Qed.

Lemma card_pred' {A} {X : finite_set_of A} (x : A) `{EqDecPoint A x} (Xx : X x) n
  : #|X| = S n -> #| (X - FSetof.singleton x)%fsetof | = n.
Proof.
  intros HX. rewrite (card_pred Xx) in HX. injection HX. auto.
Qed.

Lemma in_difference {A} (X Y : set_of A) (x : A)
  : (X - Y)%setof x <-> (X x /\ ~ Y x).
Proof.
  etransitivity; [ apply Bool.andb_true_iff | ].
  apply and_iff_compat_l, symmetry, rwP, negP.
Qed.

Lemma in_singleton {A} {x : A} `{EqDecPoint A x} (y : A) : Setof.singleton x y <-> x = y.
Proof.
  symmetry; apply rwP, sumboolP.
Defined.
