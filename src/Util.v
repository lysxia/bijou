From Coq Require Import ssreflect ssrbool Setoid.

Set Primitive Projections.
Set Implicit Arguments.

#[local] Open Scope bool_scope.

Declare Scope option_scope.
Delimit Scope option_scope with option.
Bind Scope option_scope with option.

Definition bind_option {A B} (o : option A) (k : A -> option B) : option B :=
  match o with
  | None => None
  | Some a => k a
  end.

Notation "'let?' a ':=' oa 'in' k" := (bind_option oa (fun a => k)) (at level 200) : option_scope.

Definition kleisli_option {A B C} (f : A -> option B) (g : B -> option C) : A -> option C :=
  fun a => bind_option (f a) g.

(* Use the same level as ext-lib *)
Infix ">=>" := kleisli_option (at level 61, right associativity) : option_scope.

(* Predicate transformer semantics of [option].
   Lifts "postconditions" [P : A -> Prop] to (weakest) "preconditions" [option A -> Prop]. *)
Definition prop_option {A} (oa : option A) (P : A -> Prop) : Prop :=
  match oa with
  | None => False
  | Some a => P a
  end.

Definition bool_option {A} (oa : option A) (P : A -> bool) : bool :=
  match oa with
  | None => false
  | Some a => P a
  end.

Definition prop_optionP {A} {oa : option A} {P : A -> bool}
  : reflect (prop_option oa P) (bool_option oa P).
Proof.
  destruct oa; cbn; [ apply idP | apply ReflectF; auto ].
Qed.

Definition prop_option_ind A (Q : option A -> Prop) (P : A -> Prop)
  : (forall a, P a -> Q (Some a)) ->
    forall oa, prop_option oa P -> Q oa.
Proof.
  intros HP [a | ]; cbn; [apply HP | contradiction].
Qed.

Lemma prop_option_ex {A} (oa : option A) (P : A -> Prop)
  : prop_option oa P <-> exists a, oa = Some a /\ P a.
Proof.
  split.
  - destruct oa as [ a | ]; cbn; [ exists a; auto | contradiction ].
  - intros [ ? [ -> ? ] ]; cbn; auto.
Qed.

Definition unwrap_prop_option {A} {oa : option A} {P : A -> Prop}
  : prop_option oa P -> { a : A | P a } :=
  match oa with
  | None => fun H => match H in False with end
  | Some a => exist _ a
  end.

Definition unwrap_prop_option_eq {A} {oa : option A} {P : A -> Prop}
  : forall (H : prop_option oa P), oa = Some (proj1_sig (unwrap_prop_option H)) :=
  match oa with
  | None => fun H => match H in False with end
  | Some a => fun _ => eq_refl
  end.

Definition forall_iff {A : Type} (P Q : A -> Prop)
  : (forall a, P a <-> Q a) -> (forall a, P a) <-> (forall a, Q a).
Proof.
  intros H; split; intros; apply H; auto.
Qed.

Definition f_sum {A1 B1 A2 B2} (f1 : A1 -> B1) (f2 : A2 -> B2) (x : A1 + A2) : B1 + B2 :=
  match x with
  | inl x1 => inl (f1 x1)
  | inr x2 => inr (f2 x2)
  end.

Lemma fg_sum {A1 B1 A2 B2} (f1 : A1 -> B1) g1 (f2 : A2 -> B2) g2
  : (forall b1, f1 (g1 b1) = b1) ->
    (forall b2, f2 (g2 b2) = b2) ->
    (forall b, f_sum f1 f2 (f_sum g1 g2 b) = b).
Proof.
  intros H1 H2 [b1 | b2]; cbn; f_equal; [apply H1 | apply H2].
Qed.

Definition sum_swap {A B} (x : A + B) :=
  match x with inl a => inr a | inr b => inl b end.

Definition f_prod {A1 B1 A2 B2} (f1 : A1 -> B1) (f2 : A2 -> B2) (x : A1 * A2) : B1 * B2 :=
  (f1 (fst x), f2 (snd x)).

Lemma fg_prod {A1 B1 A2 B2} (f1 : A1 -> B1) g1 (f2 : A2 -> B2) g2
  : (forall b1, f1 (g1 b1) = b1) ->
    (forall b2, f2 (g2 b2) = b2) ->
    (forall b, f_prod f1 f2 (f_prod g1 g2 b) = b).
Proof.
  intros H1 H2 [b1 b2]; cbv; f_equal; auto.
Qed.
