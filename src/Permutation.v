(** * Permutations *)

(** *)

From Coq Require Import Arith FinFun List Setoid ssreflect ssrbool.
From Equations Require Import Equations.
From Bijou Require Import Util Bijection Fin Finite Setof Decidable Sets Sigma.

Import EqNotations.

Set Implicit Arguments.
Set Contextual Implicit.
Set Maximal Implicit Insertion.

Fixpoint factorial (n : nat) : nat :=
  match n with
  | O => 1
  | S m => n * factorial m
  end.

(** A permutation of [A] is a list in which every [A] occurs exactly once. *)

Definition Once {A : Type} (x : A) (xs : list A) : Prop :=
  exists xs0 xs1, xs = xs0 ++ x :: xs1 /\ ~ In x xs0 /\ ~ In x xs1.

Definition is_permutation {A} (xs : list A) : Prop :=
  forall x, Once x xs.

Fixpoint count_occ' {A} (y : A) `{EqDecPoint A y} (xs : list A) : nat :=
  match xs with
  | nil => 0
  | x :: xs => if eq_dec_point x then S (count_occ' xs) else count_occ' xs
  end.

Arguments count_occ' : clear implicits.
Arguments count_occ' {A} y {_} xs.

Definition Once' {A : Type}  (x : A) `{EqDecPoint A x} (xs : list A) : bool :=
  count_occ' x xs =? 1.

Arguments Once' : clear implicits.
Arguments Once' {A} x {_} xs.

Definition is_permutation' {A : finite_type} (xs : list A) : bool :=
  finite_forall (fun x : A => Once' x xs).

Definition permutation_ (A : finite_type) : Type :=
  { xs : list A | is_permutation' xs }.

Record is_subpermutation {A} (X : set_of A) (xs : list A) : Prop :=
  { is_subp_once : forall x, X x -> Once x xs
  ; is_subp_elem : forall x, In x xs -> X x
  }.

Definition is_subpermutation' {A} (X : finite_set_of A) (xs : list A) : bool :=
  setof_forall X (fun (x : A) (H : _) => let _ := EqDecPoint_setof_elem X x H in Once' x xs) &&
  forallb (fun x => X x) xs.

Definition subpermutation_ {A} (X : finite_set_of A) : Type :=
  { xs : list A | is_subpermutation' X xs }.

Section Once.

Context {A : Type}.

Lemma Once_nil (x : A) : ~ Once x nil.
Proof.
  intros [xs0 [xs11 [HH _]]]; destruct xs0; discriminate.
Defined.

Lemma Once_tl (y x : A) xs : x <> y -> Once y (x :: xs) <-> Once y xs.
Proof.
  intros Hxy; split.
  - intros [xs0 [xs1 [Hxxs [H0 H1]]]].
    destruct xs0 as [| z xs0]; cbn in Hxxs; injection Hxxs.
    + intros _ ?; contradiction.
    + intros -> <-. exists xs0, xs1. split; [ reflexivity |]. split; [ | auto ].
      revert H0; apply contra_not; right; auto.
  - intros [xs0 [xs1 [Hxs [H0 H1]]]].
    exists (x :: xs0), xs1; cbn.
    split; [ f_equal; auto | ].
    split; [ | auto ].
    by intros [Exy | Ey]; auto.
Qed.

Lemma Once_hd (x : A) xs : Once x (x :: xs) <-> ~ In x xs.
Proof.
  split.
  - intros [xs0 [xs1 [H0 [Hx H1]]]].
    destruct xs0; cbn in H0; injection H0.
    + intros <-; auto.
    + intros -> <-. contradiction Hx. left; reflexivity.
  - exists nil, xs; split; auto.
Qed.

Lemma count_occ_not_In' (x : A) `{EqDecPoint A x} (l : list A)
  : ~ In x l <-> count_occ' x l = 0.
Proof.
  induction l; cbn [count_occ'].
  - split; auto.
  - unfold eq_dec_point, EqDec_EqDecPoint. destruct (H a) as [<- | Ha]; cbn [is_left].
    + split; [ | discriminate ]. intros Hx; contradiction Hx. left; auto.
    + rewrite <- IHl. rewrite not_in_cons. split; auto.
      intros []; auto.
Qed.

Lemma OnceP (x : A) `{EDP : EqDecPoint A x} (xs : list A) : reflect (Once x xs) (Once' x xs).
Proof.
  apply Bool.iff_reflect.
  induction xs; cbn.
  - split; [ | discriminate ]. intros H; contradiction (Once_nil H).
  - unfold Once'. cbn [count_occ']. unfold eq_dec_point, EqDec_EqDecPoint.
    destruct (EDP a) as [ <- | ]; cbn [is_left].
    + rewrite Once_hd. split.
      { intros H. apply /eqP /f_equal. move: H. apply count_occ_not_In'. }
      { move=> /eqP H; injection H; move: H => _. apply count_occ_not_In'. }
    + rewrite Once_tl; auto.
Qed.

End Once.

Lemma is_permutationP {A : finite_type} (xs : list A)
  : reflect (is_permutation xs) (is_permutation' xs).
Proof.
  apply Bool.iff_reflect.
  unfold is_permutation'.
  rewrite <- (Bool.reflect_iff _ _ (finite_forallP _)).
  apply forall_iff. intros; apply Bool.reflect_iff.
  apply OnceP.
Qed.

Lemma is_subpermutationP {A} {X : finite_set_of A} (xs : list A)
  : reflect (is_subpermutation X xs) (is_subpermutation' X xs).
Proof.
  apply Bool.iff_reflect.
  unfold is_subpermutation'.
  rewrite Bool.andb_true_iff.
  rewrite <- (Bool.reflect_iff _ _ (setof_forallP _ _)).
  rewrite forallb_forall.
  split; [ intros []; split; auto | intros []; constructor; auto ].
    - intros; apply /OnceP; auto.
    - intros x Hx; apply /OnceP; apply (H x Hx).
Qed.

Module Import Subpermutation.
Section Subpermutation.

Context {A : Type} {EqDecA : EqDec A}.

Lemma empty_is_subpermutation_nil (X : set_of A) : is_subpermutation X nil -> Setof.is_empty X.
Proof.
  intros H x Hx. apply H in Hx. contradiction (Once_nil Hx).
Qed.

Section Subpermutation_cons.

Context (X : set_of A) (x : A) (xs : list A).
Context (H : is_subpermutation X (x :: xs)).

Lemma hd_is_subpermutation : X x.
Proof.
  apply H; constructor; reflexivity.
Defined.

Lemma tl_is_subp_once : forall y, (X - Setof.singleton x)%setof y -> Once y xs.
Proof.
  move=> y /in_difference [HX Hx].
  eapply Once_tl. { rewrite in_singleton in Hx; eassumption. } { apply H; assumption. }
Qed.

Lemma In_tl_subpermutation : ~ In x xs.
Proof.
  apply Once_hd, H, H; left; reflexivity.
Qed.

Lemma tl_is_subp_elem : forall y, In y xs -> (X - Setof.singleton x)%setof y.
Proof.
  move=> y Hy. rewrite in_difference. split.
  - apply H; right; auto.
  - rewrite in_singleton. intros <-. apply In_tl_subpermutation. auto.
Qed.

Lemma tl_is_subpermutation : is_subpermutation (X - Setof.singleton x) xs.
Proof.
  constructor.
  - apply @tl_is_subp_once.
  - apply @tl_is_subp_elem.
Qed.

End Subpermutation_cons.

Lemma is_subpermutation_nil (X : set_of A) : Setof.is_empty X -> is_subpermutation X nil.
Proof.
  intros H. constructor.
  - intros x Hx; contradiction (H x Hx).
  - intros x Hx; contradiction Hx.
Qed.

Lemma is_subpermutation_difference_In (X : set_of A) (x : A) (xs : list A)
  : is_subpermutation (X - Setof.singleton x) xs -> ~ In x xs.
Proof.
  intros Hxs Hx. apply Hxs in Hx. move: Hx => /in_difference [ _ ].
  rewrite in_singleton; auto.
Qed.

Lemma is_subpermutation_difference_Once (X : set_of A) (y x : A) (xs : list A)
  : is_subpermutation (X - Setof.singleton x) xs -> y <> x -> X y -> Once y xs.
Proof.
  intros Hxs Hxy Hy. apply Hxs. apply in_difference. split; auto.
  rewrite in_singleton. auto.
Qed.

Lemma is_subpermutation_difference_elem {X : set_of A} {y x : A} (xs : list A)
  : is_subpermutation (X - Setof.singleton x) xs -> In y xs -> X y.
Proof.
  intros Hxs Hy. apply Hxs in Hy. by move: Hy => /in_difference [].
Qed.

Lemma is_subpermutation_cons (X : set_of A) (x : A) (xs : list A)
  : X x -> is_subpermutation (X - Setof.singleton x) xs -> is_subpermutation X (x :: xs).
Proof.
  intros Hx Hxs. constructor.
  - intros y Hy. destruct (eq_dec x y) as [<- | Hxy].
    + apply Once_hd. apply (is_subpermutation_difference_In Hxs).
    + apply Once_tl; [ | apply (is_subpermutation_difference_Once Hxs) ]; auto.
  - intros y [ <- | Hy ]; [ auto | apply (is_subpermutation_difference_elem Hxs Hy) ].
Qed.

Theorem card_pred_factorial {X : finite_set_of A} (x : A) (H : X x)
  : factorial #|X| = #|X| * factorial (#|(X - FSetof.singleton x)%fsetof|).
Proof.
  rewrite (card_pred H). reflexivity.
Qed.

Lemma is_subpermutation_S {n : nat} {X : finite_set_of A} (H : #|X| = S n)
  : is_subpermutation X nil -> False.
Proof.
  intros [H0 _].
  specialize (H0 _ (proj2_sig (bij_from X (rew <- H in FO)))).
  destruct H0 as [? [? [? _]]].
  revert H0; apply app_cons_not_nil.
Qed.

Definition subpermutation_S_to_ {n : nat} {X : finite_set_of A} (H : #|X| = S n) (xs : list A)
  : is_subpermutation X xs ->
    { x : X & subpermutation_ (X - FSetof.singleton (proj1_sig x))%fsetof } :=
  match xs with
  | nil => fun H0 => match is_subpermutation_S H H0 with end
  | x :: xs => fun H0 =>
    existT _ (exist _ x (hd_is_subpermutation H0))
      (exist _ xs (introT (is_subpermutationP (X := (X - FSetof.singleton x)%fsetof))
        (tl_is_subpermutation H0)))
  end.

Definition subpermutation_S_to {n : nat} {X : finite_set_of A} (H : #|X| = S n)
  : subpermutation_ X -> { x : X & subpermutation_ (X - FSetof.singleton (proj1_sig x))%fsetof } :=
  fun xs => subpermutation_S_to_ H (elimT is_subpermutationP (proj2_sig xs)).

Definition subpermutation_S_from_ {X : finite_set_of A}
    (x : X) (xs : list A)
  : is_subpermutation (X - FSetof.singleton (proj1_sig x))%fsetof xs ->
    subpermutation_ X :=
  fun H0 => exist _ (proj1_sig x :: xs)
    (introT is_subpermutationP (is_subpermutation_cons (proj2_sig x) H0)).

Definition subpermutation_S_from {X : finite_set_of A}
  : { x : X & subpermutation_ (X - FSetof.singleton (proj1_sig x))%fsetof } -> subpermutation_ X :=
  fun xs => subpermutation_S_from_ (projT1 xs) (elimT is_subpermutationP (proj2_sig (projT2 xs))).

Lemma subpermutation_S_is_bijection {n : nat} {X : finite_set_of A} (H : #|X| = S n)
  : is_bijection (subpermutation_S_to H) subpermutation_S_from.
Proof.
  unfold subpermutation_S_from, subpermutation_S_to; constructor; intros x; cbn.
  - destruct x as [ [x1 ] [x2] ]; cbn. unfold subpermutation_.
    exact (eq_existT_exist2'
      (Q := fun x => is_subpermutation' (X - FSetof.singleton x)%fsetof) _ _ _ _ _ _).
  - apply Setof.ssig_eq_inv. cbn.
    destruct x as [ [ | x xs ] Hxs ]; cbn.
    + destruct (_ : False).
    + reflexivity.
Qed.

Definition subpermutation_S_bij {n : nat} {X : finite_set_of A} (H : #|X| = S n)
  : subpermutation_ X <--> { x : X & subpermutation_ (X - FSetof.singleton (proj1_sig x))%fsetof } :=
  {| bij_is_bijection := subpermutation_S_is_bijection (H := H) |}.

Definition empty_subpermutation {X : finite_set_of A} (H : #|X| = 0) : subpermutation_ X :=
  exist _ nil (introT is_subpermutationP (is_subpermutation_nil (is_empty_0 H))).

Lemma subpermutation_0_unique {X : finite_set_of A} (H : #|X| = 0)
  : forall xs : subpermutation_ X, empty_subpermutation H = xs.
Proof.
  intros [ [ | x xs] ].
  - apply Setof.ssig_eq_inv; reflexivity.
  - contradiction (is_empty_0 H x).
    move: i => /is_subpermutationP. apply hd_is_subpermutation.
Qed.

Definition subpermutation_0_bij {X : finite_set_of A} (H : #|X| = 0)
  : subpermutation_ X <--> fin 1 :=
  Bij_singleton (@subpermutation_0_unique _ H).

Fixpoint enum_subpermutation_ {n : nat} {X : finite_set_of A}
  : #|X| = n -> subpermutation_ X <--> fin (factorial n) :=
  match n return _ n -> _ <--> fin (factorial n) with
  | O => subpermutation_0_bij
  | S n => fun H => subpermutation_S_bij (X := X) H >>>
    enum_sigT_const_card (bij_card (A := X) H)
      (fun x => enum_subpermutation_ (n := n) (card_pred' (proj2_sig x) H))
  end%bij.

End Subpermutation.
End Subpermutation.

Definition enum_subpermutation {A : Type} `{EqDec A} {X : finite_set_of A}
  : subpermutation_ X <--> fin (factorial #|X|) :=
  enum_subpermutation_ eq_refl.

Definition is_finite_subpermutation {A : Type} `{EqDec A} {X : finite_set_of A}
  : is_finite (subpermutation_ X) :=
  {| enum := enum_subpermutation |}.

Definition subpermutation {A : Type} `{EqDec A} (X : finite_set_of A)
  : finite_type :=
  {| ft_is_finite := is_finite_subpermutation (X := X) |}.

Lemma is_permutation_sub A {xs : list A}
  : is_permutation xs <-> is_subpermutation (Setof.top (A := A)) xs.
Proof.
  split.
  - split; intros; [ apply H | constructor ].
  - intros [H _] ?. apply H; constructor.
Qed.

Lemma is_permutation_sub' {A : finite_type} {xs : list A}
  : is_permutation' xs <-> is_subpermutation' (FSetof.top (A := A)) xs.
Proof.
  split.
  - by move=> /is_permutationP /is_permutation_sub /(is_subpermutationP (X := FSetof.top)).
  - by move=> /is_subpermutationP /is_permutation_sub /is_permutationP.
Qed.

Definition permutation_sub {A : finite_type}
  : permutation_ A <--> subpermutation_ (FSetof.top (A := A)) :=
  Bij_proj2_sig (fun _ => bij_unique_Prop is_permutation_sub'
    (fun _ _ => set_uip _ _ _ _) (fun _ _ => set_uip _ _ _ _)).

Definition enum_permutation {A : finite_type} : permutation_ A <--> fin (factorial #|A|) :=
  permutation_sub >>> enum_subpermutation.

Definition is_finite_permutation {A : finite_type} : is_finite (permutation_ A) :=
  {| enum := enum_permutation |}.

Definition permutation (A : finite_type) : finite_type :=
  {| ft_is_finite := is_finite_permutation (A := A) |}.
