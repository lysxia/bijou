From Coq Require Import ssreflect ssrbool Arith.
From Bijou Require Import Bijection Fin Finite Sets.
Import EqNotations.

Module Bij_projT1.
Section S.

Context {A B : Type} `{IsSet A} {f : A -> Type} {g : A <--> B}.

Notation SIGA := { x : A & f x }.
Notation SIGB := { y : B & f (bij_from g y) }.

Definition to (x : SIGA) : SIGB :=
  existT _ (bij_to g (projT1 x)) (rew <- (gf g (projT1 x)) in (projT2 x)).

Definition from (x : SIGB) : SIGA :=
  existT _ (bij_from g (projT1 x)) (projT2 x).

Lemma is_bijection : is_bijection to from.
Proof.
  unfold to, from; constructor; intros x; cbn.
  - apply eq_existT_l with (p := fg g _).
    unfold eq_rect_r. rewrite rew_map rew_compose.
    apply set_eq_rect_eq.
  - apply eq_existT_l with (p := gf g _).
    unfold eq_rect_r. rewrite rew_compose.
    apply set_eq_rect_eq.
Qed.

End S.
End Bij_projT1.

Definition Bij_projT1 {A B : Type} `{IsSet A} {f : A -> Type} (g : A <--> B)
  : { x : A & f x } <--> { y : B & f (bij_from g y) } :=
  {| bij_is_bijection := Bij_projT1.is_bijection |}.

Module Bij_projT2.
Section S.
Context {A : Type} {f g : A -> Type} (H : forall a, f a <--> g a).

Definition to (x : sigT f) : sigT g := existT _ _ (bij_to (H _) (projT2 x)).
Definition from (x : sigT g) : sigT f := existT _ _ (bij_from (H _) (projT2 x)).

Lemma is_bijection : is_bijection to from.
Proof.
  unfold to, from; constructor; intros []; cbn; f_equal; apply H.
Defined.

End S.
End Bij_projT2.

Definition Bij_projT2 {A : Type} {f g : A -> Type} (H : forall a, f a <--> g a)
  : sigT f <--> sigT g :=
  {| bij_is_bijection := Bij_projT2.is_bijection H |}.

Module Bij_proj1_sig.
Section S.

Context {A B : Type} `{IsSet A} {f : A -> Prop} {g : A <--> B}.

Notation SIGA := { x : A | f x }.
Notation SIGB := { y : B | f (bij_from g y) }.

Definition to (x : SIGA) : SIGB :=
  exist _ (bij_to g (proj1_sig x)) (rew <- (gf g (proj1_sig x)) in (proj2_sig x)).

Definition from (x : SIGB) : SIGA :=
  exist _ (bij_from g (proj1_sig x)) (proj2_sig x).

Lemma is_bijection : is_bijection to from.
Proof.
  unfold to, from; constructor; intros x; cbn.
  - apply eq_exist_l with (p := fg g _).
    unfold eq_rect_r. rewrite rew_map rew_compose.
    apply set_eq_rect_eq.
  - apply eq_exist_l with (p := gf g _).
    unfold eq_rect_r. rewrite rew_compose.
    apply set_eq_rect_eq.
Qed.

End S.
End Bij_proj1_sig.

Definition Bij_proj1_sig {A B : Type} `{IsSet A} {f : A -> Prop} (g : A <--> B)
  : { x : A | f x } <--> { y : B | f (bij_from g y) } :=
  {| bij_is_bijection := Bij_proj1_sig.is_bijection |}.

Module Bij_proj2_sig.
Section S.
Context {A : Type} {f g : A -> Prop} (H : forall a, f a <--> g a).

Definition to (x : sig f) : sig g := exist _ _ (bij_to (H _) (proj2_sig x)).
Definition from (x : sig g) : sig f := exist _ _ (bij_from (H _) (proj2_sig x)).

Lemma is_bijection : is_bijection to from.
Proof.
  unfold to, from; constructor; intros []; cbn; f_equal; apply H.
Defined.

End S.
End Bij_proj2_sig.

Definition Bij_proj2_sig {A : Type} {f g : A -> Prop} (H : forall a, f a <--> g a)
  : sig f <--> sig g :=
  {| bij_is_bijection := Bij_proj2_sig.is_bijection H |}.

(* Bounded sum over natural numbers *)
Definition big {A : Type} (o : A) (s : A -> A -> A) (f : nat -> A) : nat -> A :=
  fix _big n :=
    match n with
    | O => o
    | S n => s (_big n) (f n)
    end.

Definition bigsum : (nat -> nat) -> nat -> nat := big 0 Nat.add.
Definition bigsigma : (nat -> Type) -> nat -> Type := big (A := Type) False sum.
Definition bigsigma_finite : (nat -> finite_type) -> nat -> finite_type
  := big False_finite sum_finite.

Theorem cong_bigsum {f g} (H : forall i, f i = g i) {n} : bigsum f n = bigsum g n.
Proof.
  induction n; cbn; [ reflexivity | rewrite H; auto ].
Qed.

Theorem card_bigsigma_finite f n : #|bigsigma_finite f n| = bigsum (fun i => #|f i|) n.
Proof.
  induction n as [ | n IH ]; cbn; [ reflexivity | f_equal; apply IH ].
Qed.

Definition fbig {A : Type} (o : A) (s : A -> A -> A) : forall (n : nat), (fin n -> A) -> A :=
  fix _fbig n :=
    match n with
    | O => fun _ => o
    | S n => fun f => s (f FO) (_fbig n (fun i => f (FS i)))
    end.

Definition fbigsum : forall n, (fin n -> nat) -> nat := fbig 0 Nat.add.
Definition fbigsigma_finite : forall n, (fin n -> finite_type) -> finite_type :=
  fbig False_finite sum_finite.

Theorem card_fbigsigma_finite {n f} : #|fbigsigma_finite n f| = fbigsum n (fun i => #|f i|).
Proof.
  induction n as [ | n IH ]; cbn; [ reflexivity | f_equal; apply IH ].
Qed.

Definition bij_fbigsigma_finite {n f}
  : fbigsigma_finite n f <--> fin (fbigsum n (fun i => #|f i|)) :=
  bij_card card_fbigsigma_finite.

Lemma bigsum_S {n : nat} (f : nat -> nat) : bigsum f (S n) = f 0 + bigsum (fun i => f (S i)) n.
Proof.
  revert f; induction n as [| n IH]; intros; cbn.
  - rewrite Nat.add_0_r; reflexivity.
  - rewrite Nat.add_assoc; f_equal; apply IH.
Qed.

Theorem fbigsum_bigsum {n : nat} {f : fin n -> nat} {g : nat -> nat}
    (H : forall i : fin n, f i = g (Fin_to_nat i))
  : fbigsum n f = bigsum g n.
Proof.
  revert n f g H; induction n as [ | n IH ]; cbn [fbigsum fbig]; [ reflexivity | intros ].
  rewrite bigsum_S; f_equal.
  - apply H.
  - apply IH. intros. apply H.
Qed.

Module Bij_sigT_sum.
Section S.
Context {A B : Type} {f : A + B -> Type}.
Notation L := { x : A + B & f x }.
Notation R := ({x : A & f (inl x)} + {y : B & f (inr y)})%type.

Definition to (x : L) : R :=
  match projT1 x as u return f u -> R with
  | inl x => fun H => inl (existT _ x H)
  | inr x => fun H => inr (existT _ x H)
  end (projT2 x).

Definition from (x : R) : L :=
  match x with
  | inl x => existT _ (inl _) (projT2 x)
  | inr x => existT _ (inr _) (projT2 x)
  end.

Lemma is_bijection : is_bijection to from.
Proof.
  constructor; [ intros [ [] | [] ] | intros [ [] ? ] ]; reflexivity.
Qed.
End S.
End Bij_sigT_sum.

Definition Bij_sigT_sum {A B : Type} {f : A + B -> Type}
  : { x : A + B & f x } <--> {x : A & f (inl x)} + {y : B & f (inr y)} :=
  {| bij_is_bijection := Bij_sigT_sum.is_bijection |}.

Module Bij_sig_sum.
Section S.
Context {A B : Type} {f : A + B -> Prop}.
Notation L := { x : A + B | f x }.
Notation R := ({x : A | f (inl x)} + {y : B | f (inr y)})%type.

Definition to (x : L) : R :=
  match proj1_sig x as u return f u -> R with
  | inl x => fun H => inl (exist _ x H)
  | inr x => fun H => inr (exist _ x H)
  end (proj2_sig x).

Definition from (x : R) : L :=
  match x with
  | inl x => exist _ (inl _) (proj2_sig x)
  | inr x => exist _ (inr _) (proj2_sig x)
  end.

Lemma is_bijection : is_bijection to from.
Proof.
  constructor; [ intros [ [] | [] ] | intros [ [] ? ] ]; reflexivity.
Qed.
End S.
End Bij_sig_sum.

Definition Bij_sig_sum {A B : Type} {f : A + B -> Prop}
  : { x : A + B | f x } <--> {x : A | f (inl x)} + {y : B | f (inr y)} :=
  {| bij_is_bijection := Bij_sig_sum.is_bijection |}.

Lemma is_bijection_sigT_unit {A : Type}
  : is_bijection (projT2 (P := fun _ : unit => A)) (existT _ tt).
Proof.
  constructor; cbn; [ reflexivity | intros [ [] ? ]; reflexivity ].
Defined.

Definition Bij_sigT_unit {A : Type} : { x : unit & A } <--> A :=
  {| bij_is_bijection := is_bijection_sigT_unit |}.

Definition Bij_sigT_Fin_0 {f : fin 0 -> finite_type}
  : { i : fin 0 & f i } <--> fbigsigma_finite 0 f :=
  Bij_Empty (fun x => fin_case0 _ (projT1 x)) (fun x => x).

Definition Bij_sigT_Fin_S {n} {f : fin (S n) -> Type} {A : Type}
    (H : { i : fin n & f (FS i) } <--> A)
  : { i : fin (S n) & f i } <--> f FO + A :=
  Bij_projT1 (Bij_S (n := n)) >>> Bij_sigT_sum >>> Bij_sum (Bij_sigT_unit (A := f _)) H.

Fixpoint Bij_sigT_Fin {n : nat}
  : forall {f : fin n -> finite_type}, { i : fin n & f i } <--> fbigsigma_finite n f :=
  match n with
  | O => fun _ => Bij_sigT_Fin_0
  | S n => fun _ => Bij_sigT_Fin_S Bij_sigT_Fin
  end.

Section finite_sigT.

Context (A : finite_type) (f : A -> finite_type).

Definition fnbig {B} (o : B) (s : B -> B -> B) (f : A -> B) : B :=
  fbig o s #|A| (fun i => f (bij_from A i)).

Definition fnbigsum : (A -> nat) -> nat := fnbig 0 Nat.add.
Definition fnbigsigma_finite : (A -> finite_type) -> finite_type :=
  fnbig False_finite sum_finite.

Definition Bij_sigT_finite' : { x : A & f x } <--> { i : fin #|A| & f (bij_from A i) } :=
  Bij_projT1 _.

Definition enum_sigT_finite : { x : A & f x } <--> fin (fnbigsum (fun x => #|f x|)) :=
  Bij_sigT_finite' >>> Bij_sigT_Fin >>> enum_ >>> Bij_Fin_eq card_fbigsigma_finite.

Definition is_finite_sigT_finite : is_finite { x : A & f x } :=
  {| enum := enum_sigT_finite |}.

Definition sigT_finite : finite_type :=
  {| ft_is_finite := is_finite_sigT_finite |}.

End finite_sigT.

Section bounded_sigT.

Context (n : nat) (f : nat -> finite_type).
Context (H : forall k, f k -> n <= k -> False).

Definition bounded_sigT_to (x : {k & f k}) : { i : fin n & f (Fin_to_nat i) } :=
  let i := unwrap_sumor (H _ (projT2 x)) (nat_to_Fin' (projT1 x) n) in
  existT _ (proj1_sig i) (rew [f] (proj2_sig i) in (projT2 x)).

Definition bounded_sigT_from (x : { i : fin n & f (Fin_to_nat i) }) : {k & f k} :=
  existT f (Fin_to_nat _) (projT2 x).

Theorem bounded_sigT_is_bijection : is_bijection bounded_sigT_to bounded_sigT_from.
Proof.
  unfold bounded_sigT_to, bounded_sigT_from; constructor; cbn.
  - intros []. rewrite Fin_to_nat_to_Fin'; reflexivity.
  - intros []; cbn. apply (eq_existT_curried (nat_to_Fin_to_nat' _)).
    rewrite rew_compose. apply (set_eq_rect_eq x f).
Qed.

Definition bounded_sigT : { k & f k } <--> { i : fin n & f (Fin_to_nat i) } :=
  {| bij_is_bijection := bounded_sigT_is_bijection |}.

Definition enum_bounded_sigT : {k & f k} <--> fin (bigsum (fun k => #|f k|) n) :=
  bounded_sigT >>> Bij_sigT_Fin >>> bij_fbigsigma_finite
    >>> Bij_Fin_eq (fbigsum_bigsum (fun _ => eq_refl)).

End bounded_sigT.

Module sigT_const.

Definition to {A B : Type} (x : { _ : A & B }) : A * B := (projT1 x, projT2 x).
Definition from {A B : Type} (x : A * B) : { _ : A & B } := existT _ (fst x) (snd x).

Lemma is_bijection {A B} : is_bijection (@to A B) from.
Proof.
  constructor; intros []; reflexivity.
Defined.

Definition bij {A B : Type} : { _ : A & B } <--> A * B :=
  {| bij_is_bijection := is_bijection |}.

End sigT_const.

Definition enum_sigT_const_card' {A : finite_type} {B : A -> Type} {m : nat}
    (HB : forall a, B a <--> fin m) : sigT B <--> fin (#|A| * m) :=
    Bij_projT2 HB >>> Bij_projT1 (enum A) >>> sigT_const.bij >>> Bij_Fin_prod.

Definition enum_sigT_const_card {A : Type} {B : A -> Type} {n m : nat}
    (HA : A <--> fin n) (HB : forall a, B a <--> fin m) : sigT B <--> fin (n * m) :=
  enum_sigT_const_card' (A := {| ft_is_finite := {| enum := HA |} |}) HB.

Arguments enum_sigT_const_card' _ _ _ &.
Arguments enum_sigT_const_card _ _ _ _ &.

Lemma eq_existT_exist2 {A B : Type} {P : A -> bool} {Q : A -> B -> bool}
    (x y : { x : sig P & sig (Q (proj1_sig x)) })
  : forall (p : proj1_sig (projT1 x) = proj1_sig (projT1 y)),
      (proj1_sig (projT2 x) = rew p in proj1_sig (projT2 y)) -> x = y.
Proof.
  destruct x as [ [x1 Hx1] [x2 Hx2]], y as [[y1 Hy1] [y2 Hy2]]; cbn.
  intros <-; cbn; intros <-.
  rewrite (set_uip _ _ Hx1 Hy1). f_equal. f_equal. apply set_uip.
Qed.

Lemma eq_existT_exist2' {A B : Type} {P : A -> bool} {Q : A -> B -> bool}
    (a : A) (Ha1 Ha2 : P a) (b : B) (Hb1 Hb2 : Q a b)
  : existT (fun x : sig P => sig (Q (proj1_sig x))) (exist P a Ha1) (exist (Q a) b Hb1)
  = existT (fun x => _) (exist P a Ha2) (exist (Q a) b Hb2).
Proof.
  match goal with
  | [ |- ?x = ?y ] => exact (eq_existT_exist2 x y eq_refl eq_refl)
  end.
Defined.

Definition eq_exist_irrel {A} (P : A -> Prop) (H : forall x (p q : P x), p = q) (x y : sig P)
  : proj1_sig x = proj1_sig y -> x = y.
Proof.
  destruct x, y; cbn; intros <-; f_equal; apply H.
Defined.
