From Coq Require Import Fin Vector Setoid CMorphisms.
From Equations Require Import Equations.
From Bijou Require Import Util.

Set Implicit Arguments.
Set Primitive Projections.

(** * Definition *)

Record is_bijection {A B} (f : A -> B) (g : B -> A) : Prop :=
  { fg : forall b, f (g b) = b
  ; gf : forall a, g (f a) = a
  }.

Lemma is_bijection_adjunct {A B} (f : A -> B) (g : B -> A)
  : is_bijection f g <-> (forall a b, f a = b <-> a = g b).
Proof.
  split.
  - intros bb a b; split; [intros <-; symmetry | intros ->]; apply bb.
  - intros H; constructor.
    + intros b; specialize (H (g b) b). apply (proj2 H eq_refl).
    + intros a; specialize (H a (f a)). symmetry; apply (proj1 H eq_refl).
Defined.

Record Bijection A B : Type :=
  { bij_to : A -> B
  ; bij_from : B -> A
  ; bij_is_bijection :> is_bijection bij_to bij_from
  }.

Declare Scope bij_scope.
Delimit Scope bij_scope with bij.
Bind Scope bij_scope with Bijection.

Infix "<-->" := Bijection (at level 99) : type_scope.

(** * Groupoid *)

Definition Bij_id {A} : A <--> A :=
  {| bij_to := fun x => x
  ;  bij_from := fun x => x
  ;  bij_is_bijection := {| fg := fun _ => eq_refl ; gf := fun _ => eq_refl |} |}.

Definition is_bijection_dual {A B} (f : A -> B) (g : B -> A)
  : is_bijection f g -> is_bijection g f.
Proof.
  constructor; [ apply gf | apply fg ]; assumption.
Defined.

Definition Bij_dual {A B} (b : A <--> B) : B <--> A :=
  {| bij_to := bij_from b
  ;  bij_from := bij_to b
  ;  bij_is_bijection := is_bijection_dual (bij_is_bijection b)
  |}.

Definition Bij_dual_involutive {A B} (b : A <--> B)
  : Bij_dual (Bij_dual b) = b.
Proof.
  reflexivity.
Defined.

Definition is_bijection_compose {A B C} f1 g1 f2 g2
  : @is_bijection A B f1 g1 ->
    @is_bijection B C f2 g2 ->
    is_bijection (fun a => f2 (f1 a)) (fun c => g1 (g2 c)).
Proof.
  intros H1 H2. constructor.
  - intros; rewrite (fg H1), (fg H2); reflexivity.
  - intros; rewrite (gf H2), (gf H1); reflexivity.
Defined.

Definition Bij_compose {A B C} (b1 : A <--> B) (b2 : B <--> C) : A <--> C :=
  {| bij_is_bijection := is_bijection_compose b1 b2 |}.

Arguments Bij_compose _ _ _ &.

Infix ">>>" := Bij_compose (at level 50) : bij_scope.

#[global] Instance Equivalence_Bijection : Equivalence Bijection :=
  Build_Equivalence _ (@Bij_id) (@Bij_dual) (@Bij_compose).

(** * Sums *)

Lemma is_bijection_sum {A1 B1 A2 B2} f1 g1 f2 g2
  : @is_bijection A1 B1 f1 g1 ->
    @is_bijection A2 B2 f2 g2 ->
    is_bijection (f_sum f1 f2) (f_sum g1 g2).
Proof.
  intros H1 H2; constructor; (apply fg_sum; [ apply H1 | apply H2 ]).
Qed.

Definition Bij_sum {A1 B1 A2 B2}
  : Bijection A1 B1 -> Bijection A2 B2 -> Bijection (A1 + A2) (B1 + B2) := fun b1 b2 =>
  {| bij_is_bijection := is_bijection_sum b1 b2 |}.

Arguments Bij_sum _ _ _ _ &.

Infix "+" := Bij_sum : bij_scope.

Definition sum_comm_is_bijection {A B} : is_bijection (@sum_swap A B) sum_swap.
Proof.
  constructor; intros []; reflexivity.
Defined.

Definition sum_comm {A B} : A + B <--> B + A :=
  {| bij_is_bijection := sum_comm_is_bijection |}.

(** * Products *)

Lemma is_bijection_prod {A1 B1 A2 B2} f1 g1 f2 g2
  : @is_bijection A1 B1 f1 g1 ->
    @is_bijection A2 B2 f2 g2 ->
    is_bijection (f_prod f1 f2) (f_prod g1 g2).
Proof.
  intros H1 H2; constructor; (apply fg_prod; [ apply H1 | apply H2 ]).
Qed.

Definition Bij_prod {A1 B1 A2 B2}
  : Bijection A1 B1 -> Bijection A2 B2 -> Bijection (A1 * A2) (B1 * B2) := fun b1 b2 =>
  {| bij_is_bijection := is_bijection_prod b1 b2 |}.

Infix "*" := Bij_prod : bij_scope.

Module Unique.
Section S.

Context {A B : Type} (f : A -> B) (g : B -> A)
  (HA : forall a a' : A, a = a') (HB : forall b b' : B, b = b').

Lemma is_bijection : is_bijection f g.
Proof.
  constructor; intros; [ apply HB | apply HA ].
Defined.

Definition bij : A <--> B := {| bij_is_bijection := is_bijection |}.

End S.
End Unique.

Definition bij_unique_Prop {A B : Prop} (f : A <-> B)
    (HA : forall a a' : A, a = a') (HB : forall b b' : B, b = b')
  : A <--> B := Unique.bij (proj1 f) (proj2 f) HA HB.

Section Empty.

Context {A B : Type} (H : A -> False) (HB : B -> False).

Lemma is_bijection_Empty
  : is_bijection (fun x : A => match H x with end) (fun x : B => match HB x with end).
Proof.
  constructor; intros; destruct (_ : False).
Defined.

Definition Bij_Empty : Bijection A B :=
  {| bij_is_bijection := is_bijection_Empty |}.

End Empty.

Definition Bij_False {A} (H : A -> False) : Bijection A False :=
  Bij_Empty H (fun x => x).
