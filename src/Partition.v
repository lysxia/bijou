From Bijou Require Import Bijection Sets.

Section SigFiber.

Context {A B : Type} (f : A -> B).

Definition to_sig_fiber : A -> { y : B & { x : A | f x = y } } :=
  fun x => existT _ (f x) (exist _ x eq_refl).

Definition from_sig_fiber : { y : B & { x : A | f x = y } } -> A :=
  fun x => proj1_sig (projT2 x).

Definition is_bijection_sig_fiber : is_bijection to_sig_fiber from_sig_fiber.
Proof.
  unfold from_sig_fiber; constructor; [ | reflexivity ].
  intros [ ? [ ? [] ] ]; cbn; reflexivity.
Defined.

Definition sig_fiber : A <--> { y : B & { x : A | f x = y } } :=
  {| bij_is_bijection := is_bijection_sig_fiber |}.

End SigFiber.
