(* Sets: types with UIP. *)

From Coq Require Import Bool Arith Eqdep.
From Equations Require Import Equations.
From Bijou Require Import Decidable.

Import EqNotations.

Class IsSet (A : Type) : Prop :=
  set_uip : forall (x y : A) (p q : x = y), p = q.

Lemma set_uip_refl {A} `{IsSet A} (x : A) (p : x = x) : p = eq_refl.
Proof. apply set_uip. Defined.

#[global]
Instance IsSet_EqDec {A} : EqDec A -> IsSet A := Eqdep_dec.UIP_dec (A := A).

Arguments IsSet_EqDec {A _}.

#[global]
Instance IsSet_nat : IsSet nat := UIP_nat.

#[global]
Instance IsSet_bool : IsSet bool := IsSet_EqDec.

Lemma set_eq_rect_eq {U} `{IsSet U} (p : U) (Q : U -> Type) (x : Q p) (h : p = p)
  : rew [Q] h in x = x.
Proof.
  rewrite (set_uip_refl p _). reflexivity.
Qed.
